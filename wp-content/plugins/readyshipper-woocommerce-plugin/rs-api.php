<?php
/*
Plugin Name: ReadyShipper WooCommerce API
Plugin URI: http://www.readyshipper.com
Description: Enable ReadyShipper to connect to WooCommerce. Based on the JSON API plugin.
Version: 1.0.5
Author: TrueShip, LLC
Author URI: http://www.trueship.com/contact-us
*/

$dir = rs_json_api_dir();
@include_once "$dir/singletons/api.php";
@include_once "$dir/singletons/query.php";
@include_once "$dir/singletons/introspector.php";
@include_once "$dir/singletons/response.php";
@include_once "$dir/models/post.php";
@include_once "$dir/models/comment.php";
@include_once "$dir/models/category.php";
@include_once "$dir/models/tag.php";
@include_once "$dir/models/author.php";
@include_once "$dir/models/attachment.php";

function rs_json_api_init() {
  global $rs_json_api;
  if (phpversion() < 5) {
    add_action('admin_notices', 'rs_json_api_php_version_warning');
    return;
  }
  if (!class_exists('RS_JSON_API')) {
    add_action('admin_notices', 'rs_json_api_class_warning');
    return;
  }

  // Create a random code if needed.
  if (!get_option("rs_json_api_key", null)) {
    add_option("rs_json_api_key", md5(uniqid(mt_rand(), true)));
  }

  $rs_json_api = new RS_JSON_API();
}

function rs_json_api_php_version_warning() {
  echo "<div id=\"json-api-warning\" class=\"updated fade\"><p>Sorry, ReadyShipper WooCommerce API requires PHP version 5.0 or greater.</p></div>";
}

function rs_json_api_class_warning() {
  echo "<div id=\"json-api-warning\" class=\"updated fade\"><p>Oops, ReadyShipper WooCommerce API class not found.</p></div>";
}

function rs_json_api_activation() {
}

function rs_json_api_deactivation() {
}

function rs_json_api_dir() {
  return dirname(__FILE__);
}

// Add initialization and activation hooks
add_action('init', 'rs_json_api_init');
register_activation_hook("$dir/rs-api.php", 'rs_json_api_activation');
register_deactivation_hook("$dir/rs-api.php", 'rs_json_api_deactivation');
