<?php

class RS_JSON_API {

  function __construct() {
    $this->query = new RS_JSON_API_Query();
    $this->introspector = new RS_JSON_API_Introspector();
    $this->response = new RS_JSON_API_Response();

    add_action('template_redirect', array(&$this, 'template_redirect'));
    add_action('admin_menu', array(&$this, 'admin_menu'));
    add_action('pre_update_option_json_api_controllers', array(&$this, 'update_controllers'));

    $plugin_file = basename(rs_json_api_dir()) . "/rs-api.php";
    add_filter("plugin_action_links_{$plugin_file}", array(&$this, 'settings_link'));
  }

  // Add settings link on plugin page
  function settings_link($links) {
    $settings_link = '<a href="options-general.php?page=rs-json-api">Settings</a>';
    array_unshift($links, $settings_link);
    return $links;
  }

  function template_redirect() {
    // Check to see if there's an appropriate API controller + method
    $controller = strtolower($this->query->get_controller());
    $available_controllers = $this->get_controllers();
    //$enabled_controllers = explode(',', get_option('rs_json_api_controllers', 'core'));
    //$active_controllers = array_intersect($available_controllers, $enabled_controllers);
    $active_controllers = array("core");

    if ($controller) {
      if (empty($this->query->dev)) {
        error_reporting(0);
      }

      $key = get_option('rs_json_api_key', null);
      if (!$key || $key != $this->query->c) {
        $this->error("Incorrect API key '" . $this->query->c . "'.", 'forbidden', 401);
      }

      if (!in_array($controller, $active_controllers)) {
        $this->error("Unknown controller '$controller'.");
      }

      $controller_path = $this->controller_path($controller);
      if (file_exists($controller_path)) {
        require_once $controller_path;
      }
      $controller_class = $this->controller_class($controller);

      if (!class_exists($controller_class)) {
        $this->error("Unknown controller '$controller_class'.");
      }

      $this->controller = new $controller_class();
      $method = $this->query->get_method($controller);

      if ($method) {
        $this->response->setup();

        // Run action hooks for method
        do_action("rs_json_api", $controller, $method);
        do_action("rs_json_api-{$controller}-$method");

        // Error out if nothing is found
        if ($method == '404') {
          $this->error('Not found');
        }

        // Run the method
        $result = $this->controller->$method();

        // Handle the result
        $this->response->respond($result);

        // Done!
        exit;
      }
    }
  }

  function admin_menu() {
    add_options_page('ReadyShipper WooCommerce Integration Settings', 'ReadyShipper', 'manage_options', 'rs-json-api', array(&$this, 'admin_options'));
  }

  function admin_options() {
    if (!current_user_can('manage_options'))  {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    $available_controllers = $this->get_controllers();
    $active_controllers = explode(',', get_option('rs_json_api_controllers', 'core'));

    if (count($active_controllers) == 1 && empty($active_controllers[0])) {
      $active_controllers = array();
    }

    if (!empty($_REQUEST['_wpnonce']) && wp_verify_nonce($_REQUEST['_wpnonce'], "update-options")) {
      if ((!empty($_REQUEST['action']) || !empty($_REQUEST['action2'])) &&
          (!empty($_REQUEST['controller']) || !empty($_REQUEST['controllers']))) {
        if (!empty($_REQUEST['action'])) {
          $action = $_REQUEST['action'];
        } else {
          $action = $_REQUEST['action2'];
        }

        if (!empty($_REQUEST['controllers'])) {
          $controllers = $_REQUEST['controllers'];
        } else {
          $controllers = array($_REQUEST['controller']);
        }

        foreach ($controllers as $controller) {
          if (in_array($controller, $available_controllers)) {
            if ($action == 'activate' && !in_array($controller, $active_controllers)) {
              $active_controllers[] = $controller;
            } else if ($action == 'deactivate') {
              $index = array_search($controller, $active_controllers);
              if ($index !== false) {
                unset($active_controllers[$index]);
              }
            }
          }
        }
        $this->save_option('rs_json_api_controllers', implode(',', $active_controllers));
      }
      if (isset($_REQUEST['rs_json_api_key'])) {
        $this->save_option('rs_json_api_key', $_REQUEST['rs_json_api_key']);
      }
    }

    ?>
<div class="wrap">
  <div id="icon-options-general" class="icon32"><br /></div>
  <h2>ReadyShipper WooCommerce Integration Settings</h2>
  <form action="options-general.php?page=rs-json-api" method="post">
    <?php wp_nonce_field('update-options'); ?>
    <h3>Security Code</h3>
    <p>ReadyShipper requires a security code to use when communicating with WooCommerce. You may enter your own code here or accept the random code generated by default.</p>
    <table class="form-table">
      <tr valign="top">
        <td scope="row"><strong>Security code</strong></td>
        <td><code><?php bloginfo('url'); ?>?c=</code><input type="text" id="id_rs_json_api_key" name="rs_json_api_key" value="<?php echo get_option('rs_json_api_key', ''); ?>" size="32" /></td>
      </tr>
    </table>
    <p><strong>What's next?</strong>
      <span id="save_changes_note" style="display: none;">Click Save Changes.</span>
      <span id="copy_and_paste_note">Copy and paste the following into the "ReadyShipper Plugin URL" of your ReadyShipper WooCommerce module settings screen:
        <br>
        <br><code id="final_code"><?php bloginfo('url'); ?>?c=<span id='final_code_c'><?php echo get_option('rs_json_api_key', ''); ?></span></code>
      </span>
    </p>
    <p class="submit">
      <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>
    <script type="text/javascript">
        var original_code = <?php echo json_encode(get_option('rs_json_api_key', '')); ?>;
        jQuery("#id_rs_json_api_key").keyup(function() {
            var new_code = this.value;
            if (original_code == new_code) {
               jQuery("#save_changes_note").hide();
               jQuery("#copy_and_paste_note").show();
            } else {
                jQuery("#save_changes_note").show();
                jQuery("#copy_and_paste_note").hide();
                jQuery("#final_code_c").html(this.value);
            }
        });
    </script>
  </form>
</div>
<?php
  }

  function print_controller_actions($name = 'action') {
    ?>
    <div class="tablenav">
      <div class="alignleft actions">
        <select name="<?php echo $name; ?>">
          <option selected="selected" value="-1">Bulk Actions</option>
          <option value="activate">Activate</option>
          <option value="deactivate">Deactivate</option>
        </select>
        <input type="submit" class="button-secondary action" id="doaction" name="doaction" value="Apply">
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <?php
  }

  function get_method_url($controller, $method, $options = '') {
    $url = get_bloginfo('url');

    if (!empty($options) && is_array($options)) {
      $args = array();
      foreach ($options as $key => $value) {
        $args[] = urlencode($key) . '=' . urlencode($value);
      }
      $args = implode('&', $args);
    } else {
      $args = $options;
    }

    if ($controller != 'core') {
      $method = "$controller/$method";
    }

    return "$url?readyshipper=$method&$args";
  }

  function save_option($id, $value) {
    $option_exists = (get_option($id, null) !== null);
    if ($option_exists) {
      update_option($id, $value);
    } else {
      add_option($id, $value);
    }
  }

  function get_controllers() {
    $controllers = array();
    $dir = rs_json_api_dir();
    $this->check_directory_for_controllers("$dir/controllers", $controllers);
    $this->check_directory_for_controllers(get_stylesheet_directory(), $controllers);
    $controllers = apply_filters('rs_json_api_controllers', $controllers);
    return array_map('strtolower', $controllers);
  }

  function check_directory_for_controllers($dir, &$controllers) {
    $dh = opendir($dir);
    while ($file = readdir($dh)) {
      if (preg_match('/(.+)\.php$/i', $file, $matches)) {
        $src = file_get_contents("$dir/$file");
        if (preg_match("/class\s+RS_JSON_API_{$matches[1]}_Controller/i", $src)) {
          $controllers[] = $matches[1];
        }
      }
    }
  }

  function controller_is_active($controller) {
    if (defined('RS_JSON_API_CONTROLLERS')) {
      $default = RS_JSON_API_CONTROLLERS;
    } else {
      $default = 'core';
    }
    $active_controllers = explode(',', get_option('rs_json_api_controllers', $default));
    return (in_array($controller, $active_controllers));
  }

  function update_controllers($controllers) {
    if (is_array($controllers)) {
      return implode(',', $controllers);
    } else {
      return $controllers;
    }
  }

  function controller_info($controller) {
    $path = $this->controller_path($controller);
    $class = $this->controller_class($controller);
    $response = array(
      'name' => $controller,
      'description' => '(No description available)',
      'methods' => array()
    );
    if (file_exists($path)) {
      $source = file_get_contents($path);
      if (preg_match('/^\s*Controller name:(.+)$/im', $source, $matches)) {
        $response['name'] = trim($matches[1]);
      }
      if (preg_match('/^\s*Controller description:(.+)$/im', $source, $matches)) {
        $response['description'] = trim($matches[1]);
      }
      if (preg_match('/^\s*Controller URI:(.+)$/im', $source, $matches)) {
        $response['docs'] = trim($matches[1]);
      }
      if (!class_exists($class)) {
        require_once($path);
      }
      $response['methods'] = get_class_methods($class);
      return $response;
    } else if (is_admin()) {
      return "Cannot find controller class '$class' (filtered path: $path).";
    } else {
      $this->error("Unknown controller '$controller'.");
    }
    return $response;
  }

  function controller_class($controller) {
    return "rs_json_api_{$controller}_controller";
  }

  function controller_path($controller) {
    $json_api_dir = rs_json_api_dir();
    $json_api_path = "$json_api_dir/controllers/$controller.php";
    $theme_dir = get_stylesheet_directory();
    $theme_path = "$theme_dir/$controller.php";
    if (file_exists($theme_path)) {
      $path = $theme_path;
    } else if (file_exists($json_api_path)) {
      $path = $json_api_path;
    } else {
      $path = null;
    }
    $controller_class = $this->controller_class($controller);
    return apply_filters("{$controller_class}_path", $path);
  }

  function get_nonce_id($controller, $method) {
    $controller = strtolower($controller);
    $method = strtolower($method);
    return "rs_json_api-$controller-$method";
  }

  function error($message = 'Unknown error', $status = 'error', $code = 400) {
    $this->response->respond(array(
      'error' => $message
    ), $status, $code);
  }

  function include_value($key) {
    return $this->response->is_value_included($key);
  }

}
