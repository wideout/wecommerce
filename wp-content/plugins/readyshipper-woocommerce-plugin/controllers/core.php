<?php
/*
Controller name: Core
Controller description: Basic introspection methods
*/

class RS_JSON_API_Core_Controller {

  public function info() {
    global $rs_json_api;
    $php = '';
    if (!empty($rs_json_api->query->controller)) {
      return $rs_json_api->controller_info($rs_json_api->query->controller);
    } else {
      $dir = rs_json_api_dir();
      if (file_exists("$dir/rs-api.php")) {
        $php = file_get_contents("$dir/rs-api.php");
      } else {
        // Check one directory up, in case json-api.php was moved
        $dir = dirname($dir);
        if (file_exists("$dir/rs-api.php")) {
          $php = file_get_contents("$dir/rs-api.php");
        }
      }
      if (preg_match('/^\s*Version:\s*(.+)$/m', $php, $matches)) {
        $version = $matches[1];
      } else {
        $version = '(Unknown)';
      }
      $active_controllers = explode(',', get_option('rs_json_api_controllers', 'core'));
      $controllers = array_intersect($rs_json_api->get_controllers(), $active_controllers);
      return array(
        'rs_api_version' => $version,
        'controllers' => array_values($controllers)
      );
    }
  }

  public function get_recent_posts() {
    global $rs_json_api;
    $posts = $rs_json_api->introspector->get_posts();
    return $this->posts_result($posts);
  }

  public function get_posts() {
    global $rs_json_api;
    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array(
      'ignore_sticky_posts' => true
    );
    $query = wp_parse_args($url['query']);
    unset($query['readyshipper']);
    unset($query['post_status']);
    $query = array_merge($defaults, $query);
    $posts = $rs_json_api->introspector->get_posts($query);
    $result = $this->posts_result($posts);
    $result['query'] = $query;
    return $result;
  }

  public function get_post() {
    global $json_api, $post;
    $post = $json_api->introspector->get_current_post();
    if ($post) {
      $previous = get_adjacent_post(false, '', true);
      $next = get_adjacent_post(false, '', false);
      $response = array(
        'post' => new RS_JSON_API_Post($post)
      );
      if ($previous) {
        $response['previous_url'] = get_permalink($previous->ID);
      }
      if ($next) {
        $response['next_url'] = get_permalink($next->ID);
      }
      return $response;
    } else {
      $json_api->error("Not found.");
    }
  }

  public function get_page() {
    global $rs_json_api;
    extract($rs_json_api->query->get(array('id', 'slug', 'page_id', 'page_slug', 'children')));
    if ($id || $page_id) {
      if (!$id) {
        $id = $page_id;
      }
      $posts = $rs_json_api->introspector->get_posts(array(
        'page_id' => $id
      ));
    } else if ($slug || $page_slug) {
      if (!$slug) {
        $slug = $page_slug;
      }
      $posts = $rs_json_api->introspector->get_posts(array(
        'pagename' => $slug
      ));
    } else {
      $rs_json_api->error("Include 'id' or 'slug' var in your request.");
    }

    // Workaround for https://core.trac.wordpress.org/ticket/12647
    if (empty($posts)) {
      $url = $_SERVER['REQUEST_URI'];
      $parsed_url = parse_url($url);
      $path = $parsed_url['path'];
      if (preg_match('#^http://[^/]+(/.+)$#', get_bloginfo('url'), $matches)) {
        $blog_root = $matches[1];
        $path = preg_replace("#^$blog_root#", '', $path);
      }
      if (substr($path, 0, 1) == '/') {
        $path = substr($path, 1);
      }
      $posts = $rs_json_api->introspector->get_posts(array('pagename' => $path));
    }

    if (count($posts) == 1) {
      if (!empty($children)) {
        $rs_json_api->introspector->attach_child_posts($posts[0]);
      }
      return array(
        'page' => $posts[0]
      );
    } else {
      $rs_json_api->error("Not found.");
    }
  }

  public function get_date_posts() {
    global $rs_json_api;
    if ($rs_json_api->query->date) {
      $date = preg_replace('/\D/', '', $rs_json_api->query->date);
      if (!preg_match('/^\d{4}(\d{2})?(\d{2})?$/', $date)) {
        $rs_json_api->error("Specify a date var in one of 'YYYY' or 'YYYY-MM' or 'YYYY-MM-DD' formats.");
      }
      $request = array('year' => substr($date, 0, 4));
      if (strlen($date) > 4) {
        $request['monthnum'] = (int) substr($date, 4, 2);
      }
      if (strlen($date) > 6) {
        $request['day'] = (int) substr($date, 6, 2);
      }
      $posts = $rs_json_api->introspector->get_posts($request);
    } else {
      $rs_json_api->error("Include 'date' var in your request.");
    }
    return $this->posts_result($posts);
  }

  public function get_category_posts() {
    global $rs_json_api;
    $category = $rs_json_api->introspector->get_current_category();
    if (!$category) {
      $rs_json_api->error("Not found.");
    }
    $posts = $rs_json_api->introspector->get_posts(array(
      'cat' => $category->id
    ));
    return $this->posts_object_result($posts, $category);
  }

  public function get_tag_posts() {
    global $rs_json_api;
    $tag = $rs_json_api->introspector->get_current_tag();
    if (!$tag) {
      $rs_json_api->error("Not found.");
    }
    $posts = $rs_json_api->introspector->get_posts(array(
      'tag' => $tag->slug
    ));
    return $this->posts_object_result($posts, $tag);
  }

  public function get_author_posts() {
    global $rs_json_api;
    $author = $rs_json_api->introspector->get_current_author();
    if (!$author) {
      $rs_json_api->error("Not found.");
    }
    $posts = $rs_json_api->introspector->get_posts(array(
      'author' => $author->id
    ));
    return $this->posts_object_result($posts, $author);
  }

  public function get_search_results() {
    global $rs_json_api;
    if ($rs_json_api->query->search) {
      $posts = $rs_json_api->introspector->get_posts(array(
        's' => $rs_json_api->query->search
      ));
    } else {
      $rs_json_api->error("Include 'search' var in your request.");
    }
    return $this->posts_result($posts);
  }

  public function get_date_index() {
    global $rs_json_api;
    $permalinks = $rs_json_api->introspector->get_date_archive_permalinks();
    $tree = $rs_json_api->introspector->get_date_archive_tree($permalinks);
    return array(
      'permalinks' => $permalinks,
      'tree' => $tree
    );
  }

  public function get_category_index() {
    global $rs_json_api;
    $args = null;
    if (!empty($rs_json_api->query->parent)) {
      $args = array(
        'parent' => $rs_json_api->query->parent
      );
    }
    $categories = $rs_json_api->introspector->get_categories($args);
    return array(
      'count' => count($categories),
      'categories' => $categories
    );
  }

  public function get_tag_index() {
    global $rs_json_api;
    $tags = $rs_json_api->introspector->get_tags();
    return array(
      'count' => count($tags),
      'tags' => $tags
    );
  }

  public function get_author_index() {
    global $rs_json_api;
    $authors = $rs_json_api->introspector->get_authors();
    return array(
      'count' => count($authors),
      'authors' => array_values($authors)
    );
  }

  public function get_page_index() {
    global $rs_json_api;
    $pages = array();
    $post_type = $rs_json_api->query->post_type ? $rs_json_api->query->post_type : 'page';

    // Thanks to blinder for the fix!
    $numberposts = empty($rs_json_api->query->count) ? -1 : $rs_json_api->query->count;
    $wp_posts = get_posts(array(
      'post_type' => $post_type,
      'post_parent' => 0,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'numberposts' => $numberposts
    ));
    foreach ($wp_posts as $wp_post) {
      $pages[] = new RS_JSON_API_Post($wp_post);
    }
    foreach ($pages as $page) {
      $rs_json_api->introspector->attach_child_posts($page);
    }
    return array(
      'pages' => $pages
    );
  }

  public function get_nonce() {
    global $rs_json_api;
    extract($rs_json_api->query->get(array('controller', 'method')));
    if ($controller && $method) {
      $controller = strtolower($controller);
      if (!in_array($controller, $rs_json_api->get_controllers())) {
        $rs_json_api->error("Unknown controller '$controller'.");
      }
      require_once $rs_json_api->controller_path($controller);
      if (!method_exists($rs_json_api->controller_class($controller), $method)) {
        $rs_json_api->error("Unknown method '$method'.");
      }
      $nonce_id = $rs_json_api->get_nonce_id($controller, $method);
      return array(
        'controller' => $controller,
        'method' => $method,
        'nonce' => wp_create_nonce($nonce_id)
      );
    } else {
      $rs_json_api->error("Include 'controller' and 'method' vars in your request.");
    }
  }

  protected function get_object_posts($object, $id_var, $slug_var) {
    global $rs_json_api;
    $object_id = "{$type}_id";
    $object_slug = "{$type}_slug";
    extract($rs_json_api->query->get(array('id', 'slug', $object_id, $object_slug)));
    if ($id || $$object_id) {
      if (!$id) {
        $id = $$object_id;
      }
      $posts = $rs_json_api->introspector->get_posts(array(
        $id_var => $id
      ));
    } else if ($slug || $$object_slug) {
      if (!$slug) {
        $slug = $$object_slug;
      }
      $posts = $rs_json_api->introspector->get_posts(array(
        $slug_var => $slug
      ));
    } else {
      $rs_json_api->error("No $type specified. Include 'id' or 'slug' var in your request.");
    }
    return $posts;
  }

  protected function posts_result($posts) {
    global $wp_query;
    return array(
      'count' => count($posts),
      'count_total' => (int) $wp_query->found_posts,
      'pages' => $wp_query->max_num_pages,
      'posts' => $posts
    );
  }

  protected function posts_object_result($posts, $object) {
    global $wp_query;
    // Convert something like "RS_JSON_API_Category" into "category"
    $object_key = strtolower(substr(get_class($object), 12));
    return array(
      'count' => count($posts),
      'pages' => (int) $wp_query->max_num_pages,
      $object_key => $object,
      'posts' => $posts
    );
  }

  /**************************************************************************************************
  * ReadyShipper functions on top of Wordpress JSON API
  **************************************************************************************************/

  /**
   * Get an order by order ID / custom order number.  Returns the order if
   * one can be found, or false
   *
   * @since 1.0
   * @param string|int $order_number customer order number or Order ID
   * @throws Exception
   * @return object WC_Order order object if found, otherwise false
   */
  private function get_order( $order_number ) {
    // try to get order by order ID
    $order = new WC_Order( $order_number );

    // found order by ID
    if ( is_object( $order ) )
      return $order;

    // try to search for the order by custom order number
    $posts = get_posts(array(
      'numberposts' => 1,
      'meta_key'    => '_order_number',
      'meta_value'  => $order_number,
      'post_type'   => 'shop_order',
      'post_status' => 'publish',
      'fields'      => 'ids'
    ) );

    // get the single order ID
    list( $order_id ) = ( empty( $posts ) ) ? null : $posts;

    // last try
    if ( null !== $order_id )
      return new WC_Order( $order_id );

    throw new Exception( sprintf( __( 'Order with number "%s" not found', 'wc-xml-rpc-api' ), $order_number ), 404 );
  }


  public function get_shop_order_stubs() {
    global $rs_json_api;

	return $rs_json_api->introspector->get_shop_order_stubs($rs_json_api->query->since, stripcslashes($rs_json_api->query->order_status));
  }

  public function get_option_value() {
    global $rs_json_api;

    $option_value = get_option($rs_json_api->query->option_name, '');
    return array('value' => $option_value);
  }

  public function update_shipment_information() {
    global $rs_json_api;

        $rs_json_api->query->decodePostData();

	$order_number = $rs_json_api->query->order_id;
	$order_status = 'completed';
	$message = $rs_json_api->query->message;
	$date_shipped = $rs_json_api->query->date;
	$tracking_provider = $rs_json_api->query->provider;
	$tracking_number = $rs_json_api->query->tracking_number;
	$url = $rs_json_api->query->url;

	$order = $this->get_order( $order_number );
	//update_post_meta( $order->id, 'ywot_pick_up_date', strtotime( $date_shipped ) );
	update_post_meta( $order->id, 'ywot_carrier_name', woocommerce_clean( $tracking_provider ) );
	update_post_meta( $order->id, 'ywot_tracking_code',   woocommerce_clean( $tracking_number ) );
	update_post_meta( $order->id, 'ywot_picked_up',   "on" );
	update_post_meta( $order->id, '_custom_tracking_link',   woocommerce_clean( $url ) );
	/*update_post_meta( $order->id, '_date_shipped', strtotime( $date_shipped ) );
	update_post_meta( $order->id, '_tracking_provider', woocommerce_clean( $tracking_provider ) );
	update_post_meta( $order->id, '_tracking_number',   woocommerce_clean( $tracking_number ) );
	update_post_meta( $order->id, '_custom_tracking_link',   woocommerce_clean( $url ) );*/
    // Only update status after setting the tracking number - this ensures that any auto-email has the status
    // tracking number available.
    $order->update_status( $order_status, $message );

	$this->success = array();
	$this->success['rs_shipment_update'] = "1";
	return $this->success;
  }
  /**************************************************************************************************
  * ReadyShipper functions on top of Wordpress JSON API - end here
  **************************************************************************************************/

}
