<?php
/*
@version 3.0.0
*/

if(!defined('ABSPATH')) {
    exit;
}
?>
<?php get_sidebar('profile-left'); ?>

<?php $list = wc_get_order( $order->id ); //important for Google Tag Manager don't remove! ?>


<?php
global $vendor_id_thankyou;
$vendor_id_thankyou = $list->post->post_author;

global $wpdb;
$myrows = $wpdb->get_results( "SELECT meta_value FROM wp_usermeta WHERE user_id = " . $vendor_id_thankyou . " AND meta_key = '_vendor_page_title'");

if(empty($myrows[0]->meta_value))
{
	$myrows = $wpdb->get_results( "SELECT meta_value FROM wp_usermeta WHERE user_id = " . $vendor_id_thankyou . " AND meta_key = '_vendor_company'");
	
}
$store_name = $myrows[0]->meta_value;



//echo "<pre>" . print_r($vendor_id,1) . "</pre>";

?>

<div class="column fivecol">
	<div class="element-title">
		<h1><?php _e('View Order', 'makery' ); ?></h1>
	</div>
	<?php if($order){ ?>
		<?php if ( $order->has_status( 'failed' ) ) : ?>
			<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'makery' ); ?></p>
			<p><?php
				if ( is_user_logged_in() )
					_e( 'Please attempt your purchase again or go to your account page.', 'makery' );
				else
					_e( 'Please attempt your purchase again.', 'makery' );
			?></p>
			<p>
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'makery' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>" class="button pay"><?php _e( 'My Account', 'makery' ); ?></a>
				<?php endif; ?>
			</p>
		<?php else : ?>
			<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'makery' ), $order ); ?></p>
		<?php endif; ?>
		<div class="method_details">
			<?php do_action('woocommerce_thankyou_' . $order->payment_method, $order->id); ?>
			<?php do_action('woocommerce_thankyou', $order->id); ?>
		</div>
	<?php } else { ?>
		<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'makery' ), null ); ?></p>
	<?php } ?>
</div>
<?php remove_filter('the_title', 'wc_page_endpoint_title'); ?>
<?php get_sidebar('profile-right'); ?>









<script>

window.dataLayer = window.dataLayer || []
dataLayer.push({
	'event': 'orderComplete',
   'transactionId': <?php echo $order->get_order_number();?>,
   'transactionAffiliation': <?php echo "'" . $store_name . "'";?>,
   'transactionTotal': <?php echo $order->get_total();?>,
   //'transactionTax': 1.29,
  // 'transactionShipping': 5,
   'transactionProducts': [
   
   <?php foreach( $list->get_items() as $item_id => $item ) : ?>
   {
       'sku': '<?php echo $item["item_meta"]["_product_id"][0]; ?>',
       'name': '<?php echo $item["name"]; ?>',
       //'category': 'Apparel',
       'price': <?php echo $item["item_meta"]["_line_total"][0]; ?>,
       'quantity': '<?php echo $item["item_meta"]["_qty"][0]; ?>'
   },
   <?php endforeach; ?>


   ]
});
</script>