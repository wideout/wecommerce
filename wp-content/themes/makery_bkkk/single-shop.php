<?php get_header(); ?>

<div class="woocommerce">
	<div class="featured-wrap">
		<section class="site-featured container clearfix">
			<?php get_template_part('template', 'shop'); ?>
		</section>
	</div>
	<?php	
	$woocommerce_loop['single']=true;
	$woocommerce_loop['columns']=4;	
	
	ThemexWoo::queryProducts();
		
	$layout='full';
	$shop=$post->ID;
	
	ThemexWoo::getTemplate('archive-product.php');
	?>
</div>
<?php get_footer(); ?>