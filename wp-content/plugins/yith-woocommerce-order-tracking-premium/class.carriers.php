<?php
/*
 * For customize carriers list parameters and be safe about the plugin updates, avoiding loosing your custom values,
 * create a file named yith-carriers-parameters.php on root folder of the plugin and fill it as the following example :
 * <?php
*
 *  Overwrite $carriers_list array standard values
 * Add your parameter as key and modify
 *
$carriers_parameter = array(
	'TNT' => array( // use the same key as the $carriers_list array
		'track_url' => 'http://www.tnt.com/webtracker/tracking.do?cons=[TRACK_CODE]&{CHANGED_VALUE}trackType=CON&saveCons=Y' // set your custom value
	),
	'DHL'   => array(
			'name'      => 'DHL',
			'track_url' => 'http://www.dhl.com/content/g0/en/{CHANGED_VALUE}/tracking.shtml?brand=DHL&AWB=[TRACK_CODE]'
		)
);

In this way the carrier parameter will be overwritten by this custom values
*/

if ( ! defined ( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists ( 'Carriers' ) ) {
    /**
     * Implements features of Yit WooCommerce Order Tracking
     *
     * @class   Carriers
     * @package Yithemes
     * @since   1.0.0
     * @author  Lorenzo Giuffrida
     */
    class Carriers {
        /**
         * @var array   Carriers that are mandatory and will not be listed on carriers custom option
         */
        protected $carriers_hidden = array ( 'UNKNOWN' );

        protected $carriers_list = array (
            'UNKNOWN'          => array (
                'name'      => 'No carrier selected',
                'track_url' => '',
            ),
            'A1_INT'           => array (
                'name'      => 'A-1 International',
                'track_url' => 'http://www.aoneonline.com/pages/customers/shiptrack.php?tracking_number=[TRACK_CODE]',
            ),
            'AFL'              => array (
                'name'      => 'AFL Logistics Limited',
                'track_url' => 'http://184.168.75.121:82/TblTrackandTrace.aspx?ID=1&cat=[TRACK_CODE]',
            ),
            'AFGHAN'           => array (
                'name'      => 'Afghan POST',
                'track_url' => 'http://track.afghanpost.gov.af/index.php?ID=[TRACK_CODE]&Submit=Submit',
            ),
            'ANPOST_IRELAND'   => array (
                'name'      => 'AN POST IRELAND',
                'track_url' => 'http://track.anpost.ie/tracklist.aspx?track=[TRACK_CODE]',
            ),
            'ARAMEX'           => array (
                'name'      => 'Aramex',
                'track_url' => 'http://www.aramex.com/track_results_multiple.aspx?ShipmentNumber=[TRACK_CODE]',
            ),
            'AUSTRALIA_POST'   => array (
                'name'      => 'Australia POST Domestic',
                'track_url' => 'http://auspost.com.au/track/display.asp?type=article&id=[TRACK_CODE]',
            ),
            'BRT'              => array (
                'name'      => 'BRT Courier Express',
                'track_url' => 'http://as777.brt.it/vas/sped_det_show.htm?brtCode=[TRACK_CODE]&RicercaBrtCode=Ricerca&referer=sped_numspe_par.htm&nSpediz=[TRACK_CODE]',
            ),
            'BLAZEFLASH'       => array (
                'name'      => 'BlazeFlash',
                'track_url' => 'http://www.blazeflash.net/trackdetail.aspx?awbno=[TRACK_CODE]',
            ),
            'BLUE_DART'        => array (
                'name'      => 'Blue Dart',
                'track_url' => 'http://www.bluedart.com/servlet/RoutingServlet?handler=tnt&action=awbquery&awb=awb&numbers=[TRACK_CODE]',
            ),
            'BPOST'            => array (
                'name'      => 'Bpost',
                'track_url' => 'https://www.bpost.be/nl/track/',
                'form'      => '<form name="BPOST" target="_blank" method="post" action="http://track.bpost.be/etr/light/performSearch.do"><input type="hidden" name="searchByCustomerReference" value="true"><input type="hidden" name="customerReference" maxlength="50" size="40" value="[TRACK_CODE]"></form>',
            ),
            'CEVA'             => array (
                'name'      => 'CEVA',
                'track_url' => 'http://www.cevalogistics.com/en-US/toolsresources/Pages/CEVATrak.aspx?sv=[TRACK_CODE]',
            ),
            'CANADA_POST'      => array (
                'name'      => 'Canada POST',
                'track_url' => 'https://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=[TRACK_CODE]&LOCALE=en',
            ),
            'CHINA_POST'       => array (
                'name'      => 'China POST',
                'track_url' => 'http://intmail.183.com.cn/item/itemStatusQuery.do?lan=0&itemNo=[TRACK_CODE]',
            ),
            'CART2INDIA'       => array (
                'name'      => 'Chronos Couriers',
                'track_url' => 'http://chronoscouriers.com/popup/scr_popup_trak_shipment.php?shipmentId=[TRACK_CODE]',
            ),
            'CITY_LINK'        => array (
                'name'      => 'City link',
                'track_url' => 'http://www.city-link.co.uk/consignments/routing.php?parcel_ref_num=[TRACK_CODE]',
            ),
            'COLLECTPLUS'      => array (
                'name'      => 'Collect+',
                'track_url' => 'https://www.collectplus.co.uk/track/[TRACK_CODE]',
            ),
            'TRACK_TRACE'      => array (
                'name'      => 'Track & Trace by CourierPost',
                'track_url' => 'http://trackandtrace.courierpost.co.nz/search/[TRACK_CODE]',
            ),
            'DHL'              => array (
                'name'      => 'DHL',
                'track_url' => 'http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB=[TRACK_CODE]',
            ),
            'DHL_DE'           => array (
                'name'      => 'DHL Germany',
                'track_url' => 'http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc=[TRACK_CODE]&rfn=&extendedSearch=true',
            ),
            'DPD'              => array (
                'name'      => 'DPD',
                'track_url' => 'https://tracking.dpd.de/cgi-bin/delistrack?pknr=[TRACK_CODE]&typ=1&lang=en',
            ),
            'DYNAMEX'          => array (
                'name'      => 'Dynamex',
                'track_url' => 'https://direct.dynamex.com/dxnow5/track/externalTracking.jsp?ctl=[TRACK_CODE]',
            ),
            'ELTA'             => array (
                'name'      => 'Elta courier',
                'track_url' => 'http://www.eltacourier.gr/en/webservice_client.php?br=[TRACK_CODE]',
            ),
            'ENSENDA'          => array (
                'name'      => 'Ensenda',
                'track_url' => 'http://www.ensenda.com/content/track-shipment?trackingNumber=[TRACK_CODE]&TRACKING_SEND=GO',
            ),
            'FEDEX'            => array (
                'name'      => 'FEDEX',
                'track_url' => 'https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=[TRACK_CODE]&locale=en_US&cntry_code=us',
            ),
            'SUPASWIFT_FEDEX'  => array (
                'name'      => 'Supaswift - Fedex',
                'track_url' => 'https://www.supaswift.com/fx/PublicTracking/tabid/94/waybill/[TRACK_CODE]/Default.aspx',
            ),
            'FASTWAY'          => array (
                'name'      => 'Fastway.co.nz',
                'track_url' => 'http://fastway.co.nz/courier-services/track-your-parcel?l=[TRACK_CODE]',
            ),
            'FASTWAY_CO_ZA'    => array (
                'name'      => 'Fastway.co.za',
                'track_url' => 'http://www.fastway.co.za/our-services/track-your-parcel?l=[TRACK_CODE]',
            ),
            'F_FLIGHT'         => array (
                'name'      => 'First Flight',
                'track_url' => 'http://firstflight.net:8081/single-web-tracking/singleTracking.do?consNo=[TRACK_CODE]',
            ),
            'FLYKING'          => array (
                'name'      => 'FlyKing',
                'track_url' => 'http://www.flykingonline.com/WebFCS/cnotequery.aspx?cnoteno=[TRACK_CODE]',
            ),
            'GATI'             => array (
                'name'      => 'GATI',
                'track_url' => 'http://www.gati.com/http://www.gati.com/single_dkt_track_int.jsp?dktChoice=docketno&dktNo=[TRACK_CODE]',
            ),
            'GD_EX'            => array (
                'name'      => 'GD Express',
                'track_url' => 'http://intranet.gdexpress.com/official/etracking.php?capture=[TRACK_CODE]&Submit=Track',
            ),
            'HERMES'           => array (
                'name'      => 'Hermes',
                'track_url' => 'https://www.hermes-europe.co.uk/tracker.html?trackingNumber=[TRACK_CODE][0]&Postcode=[TRACK_CODE][1]',
            ),
            'YODEL'            => array (
                'name'      => 'YODEL DIRECT',
                'track_url' => 'https://www.yodel.co.uk/tracking/[TRACK_CODE]',
            ),
            'HONGKONG'         => array (
                'name'      => 'HongKong POST',
                'track_url' => 'http://app3.hongkongpost.com/CGI/mt/genresult.jsp?tracknbr=[TRACK_CODE]',
            ),
            'ICC'              => array (
                'name'      => 'ICC Worldwide',
                'track_url' => 'http://www.iccworld.com/track.asp?txtawbno=[TRACK_CODE]',
            ),
            'INDIA_POST'       => array (
                'name'      => 'India POST',
                'track_url' => 'http://www.indiapost.gov.in?articlenumber=[TRACK_CODE]',
            ),
            'INTERLINK'        => array (
                'name'      => 'InterLink Express',
                'track_url' => 'http://www.interlinkexpress.com/tracking/trackingSearch.do?search.searchType=0&appmode=guest&search.parcelNumber=[TRACK_CODE]',
            ),
            'JNE'              => array (
                'name'      => 'JNE',
                'track_url' => 'http://www.jne.co.id/index.php?mib=tracking.detail&awb=[TRACK_CODE]',
            ),
            'JAPAN_POST'       => array (
                'name'      => 'Japan POST',
                'track_url' => 'http://tracking.post.japanpost.jp/service/singleSearch.do?searchKind=S004&locale=en&reqCodeNo1=[TRACK_CODE]&x=16&y=15',
            ),
            'LAPOSTE'          => array (
                'name'      => 'LA Poste',
                'track_url' => 'http://www.csuivi.courrier.laposte.fr/suivi/index/id/[TRACK_CODE]',
            ),
            'LASERSHIP'        => array (
                'name'      => 'LaserShip',
                'track_url' => 'http://www.lasership.com/track.php?track_number_input=[TRACK_CODE]&Submit=Track',
            ),
            'MAGYAR_POSTA'     => array (
                'name'      => 'Magyar posta',
                'track_url' => 'http://posta.hu/mpecom-sa-nyomkoveto-ui/nyomkoveto.jsf?nyomkoveto:documentnumber:input=[TRACK_CODE]',
            ),
            'NAPAREX'          => array (
                'name'      => 'NAPAREX',
                'track_url' => 'https://xcel.naparex.com/orders/WebForm/OrderTracking.aspx?OrderTrackingID=[TRACK_CODE]',
            ),
            'NZ_COURIERS'      => array (
                'name'      => 'New Zealand Couriers',
                'track_url' => 'http://www.nzcouriers.co.nz/nzc/servlet/ITNG_TAndTServlet?page=1&VCCA=Enabled&Key_Type=Ticket&product_code=[TRACK_CODE][0]&serial_number=[TRACK_CODE][1]',
            ),
            'NZ_POST'          => array (
                'name'      => 'New Zealand Post',
                'track_url' => 'http://www.nzpost.co.nz/tools/tracking?trackid=[TRACK_CODE]',
            ),
            'OM_INTERNATIONAL' => array (
                'name'      => 'OM International Courier & Cargo',
                'track_url' => 'http://track.omintl.net/tracking.aspx?txtawbno=[TRACK_CODE]&submit=Submit',
            ),
            'ONTRAC'           => array (
                'name'      => 'OnTrac',
                'track_url' => 'http://www.ontrac.com/trackingres.asp?tracking_number=[TRACK_CODE]&x=11&y=6',
            ),
            'ORBIT'            => array (
                'name'      => 'Orbit Worldwide express',
                'track_url' => 'http://www.orbitexp.com/tools/showTrack.asp?awbnoMul=[TRACK_CODE]',
            ),
            'OCS'              => array (
                'name'      => 'OCS Worlwide',
                'track_url' => 'https://www.ocsworldwide.co.uk/Tracking.aspx?cwb=[TRACK_CODE]',
            ),
            'POST_LT'          => array (
                'name'      => 'Lietuvos pastas',
                'track_url' => 'http://www.post.lt/en/help/parcel-search/index?num=[TRACK_CODE]',
            ),
            'PERUPOST'         => array (
                'name'      => 'PERU POST',
                'track_url' => 'http://clientes.serpost.com.pe/prj_tracking/seguimientolinea.aspx?txtTracking=[TRACK_CODE]',
            ),
            'POS_INDONESIA'    => array (
                'name'      => 'POS Indonesia',
                'track_url' => 'http://www.posindonesia.co.id/addons/Lacak-Kiriman/modules/v_getlist.php?q=showData001&kiriman=[TRACK_CODE]',
            ),
            'POS_MALAYSIA'     => array (
                'name'      => 'POS Malaysia',
                'track_url' => 'http://pos.com.my/trackandtrace/trackandtrace/?trackNo=[TRACK_CODE]',
            ),
            'POST_NL'          => array (
                'name'      => 'POST NL',
                'track_url' => 'https://jouw.postnl.nl/#!/track-en-trace/[TRACK_CODE][0]/NL/[TRACK_CODE][1]',
            ),
            'POST_OFF_UK'      => array (
                'name'      => 'POST Office UK',
                'track_url' => 'http://www2.postoffice.co.uk/track-trace?track_id=[TRACK_CODE]&op=Track',
            ),
            'PAK_POST'         => array (
                'name'      => 'Pakistan POST',
                'track_url' => 'http://ep.gov.pk/track.asp?textfield=[TRACK_CODE]',
            ),
            'PARCEL2GO'        => array (
                'name'      => 'Parcel2Go',
                'track_url' => 'https://www1.parcel2go.com/tracking/[TRACK_CODE]',
            ),
            'PARCELFORCE'      => array (
                'name'      => 'Parcel Force',
                'track_url' => 'http://www.parcelforce.com/track-trace?parcel_tracking_number=[TRACK_CODE]',
            ),
            'PARCELLINK'       => array (
                'name'      => 'Parcel link',
                'track_url' => 'http://www.parcel-link.co.uk/track-and-trace.php?consignment=[TRACK_CODE]',
            ),
            'POS_LAJU'         => array (
                'name'      => 'Pos Laju National Courier',
                'track_url' => 'http://www.poslaju.com.my/track-trace/#trackingIds=[TRACK_CODE]',
            ),
            'POST_DAN'         => array (
                'name'      => 'Post Danmark',
                'track_url' => 'http://www.postdanmark.dk/tracktrace/TrackTrace.do?i_lang=INE&i_stregkode=[TRACK_CODE]',
            ),
            'POSTEN_NORWAY'    => array (
                'name'      => 'Posten Norway',
                'track_url' => 'http://sporing.posten.no/sporing.html?q=[TRACK_CODE]&lang=en',
            ),
            'POSTEN_SWEDEN'    => array (
                'name'      => 'Posten Sweden',
                'track_url' => 'http://www.posten.se/tracktrace/TrackConsignments_do.jsp?trackntraceAction=saveSearch&logisticCustomerNumber=&referenceNumber=&lang=GB&loginHandlerImplClass=se.posten.pse.framework.security.applicationImpl.tracktrace.LoginHandlerImpl&internalPageNumber=0&doNotShowInHistory=true&consignmentId=[TRACK_CODE]&consignmentId=&consignmentId=&consignmentId=&consignmentId=&consignmentId=&consignmentId=&consignmentId=',
            ),
            'POST_SWI'         => array (
                'name'      => 'Post Switzerland',
                'track_url' => 'http://www.post.ch/swisspost-tracking?formattedParcelCodes={POST_SWI_customer_number}[TRACK_CODE]&p_language=de',
            ),
            'TPC_INDIA'        => array (
                'name'      => 'The Professional Courier - India',
                'track_url' => 'http://www.tpcindia.com/Tracking2014.aspx?id=[TRACK_CODE]&type=0&service=0',
            ),
            'PUROLATOR'        => array (
                'name'      => 'Purolator',
                'track_url' => 'https://www.purolator.com/en/ship-track/tracking-summary.page?submit=true&componentID=1359477580240&search=[TRACK_CODE]',
            ),
            'POST_ROMANA'      => array (
                'name'      => 'Poșta Română',
                'track_url' => 'http://www.posta-romana.ro/en/posta-romana/servicii-online/track-trace.html?track=[TRACK_CODE]',
            ),
            'ROYAL_MAIL'       => array (
                'name'      => 'Royal Mail',
                'track_url' => 'http://www.royalmail.com/portal/rm/track?trackNumber=[TRACK_CODE]',
            ),
            'SM_COURIERS'      => array (
                'name'      => 'SM Couriers',
                'track_url' => 'http://www.smcouriers.com/YourShipmentDetails.aspx?reqCode=showTrackYourCosignmentPage&searchType=AWBNO&searchValue=[TRACK_CODE]',
            ),
            'SAFEXPRESS'       => array (
                'name'      => 'SafeXpress',
                'track_url' => 'http://www.safexpress.com/shipment_inq.aspx?sno=[TRACK_CODE]',
            ),
            'SHREE_MARUTI'     => array (
                'name'      => 'Shree Maruti',
                'track_url' => 'http://erp.shreemarutionline.com/frmTrackingDetails.aspx?id=[TRACK_CODE]',
            ),
            'SINGAPORE_POST'   => array (
                'name'      => 'Singapore POST',
                'track_url' => 'http://www.singpost.com/track-items?track_items=[TRACK_CODE]',
            ),
            'SKYNET'           => array (
                'name'      => 'SkyNET',
                'track_url' => 'http://www.courierworld.com/scripts/webcourier1.dll/TrackingResultwoheader?type=4&nid=1&hawbNoList=[TRACK_CODE]',
            ),
            'SPEE_DEE'         => array (
                'name'      => 'Spee-Dee',
                'track_url' => 'http://packages.speedeedelivery.com/packages.asp?tracking=[TRACK_CODE]',
            ),
            'TNT'              => array (
                'name'      => 'TNT',
                'track_url' => 'http://www.tnt.com/webtracker/tracking.do?cons=[TRACK_CODE]&trackType=CON&saveCons=Y',
            ),
            'TRAD_EX'          => array (
                'name'      => 'TradEx',
                'track_url' => 'http://www.tradelinkinternational.co.in/track.asp?awbno=[TRACK_CODE]',
            ),
            'UPS'              => array (
                'name'      => 'UPS',
                'track_url' => 'http://wwwapps.ups.com/WebTracking/processRequest?&tracknum=[TRACK_CODE]',
            ),
            'USPS'             => array (
                'name'      => 'USPS',
                //'track_url' => 'http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=[TRACK_CODE]',
		'track_url' => 'http://www.endicia.com/status/?PIC=[TRACK_CODE]',
            ),
            'URGENT_AIR'       => array (
                'name'      => 'Urgent Air Courier Express',
                'track_url' => 'http://urgentair.co.in/tracking.aspx?txtawbno=[TRACK_CODE]#tk',
            ),
            'ESTAFETA'         => array (
                'name'      => 'Estafeta',
                'track_url' => 'http://rastreo3.estafeta.com/RastreoWebInternet/consultaEnvio.do?guias=[TRACK_CODE]',
                'form'      => '<form target="_blank" name="ESTAFETA" method="post" action="http://www.estafeta.com//Rastreo/Resultados/"><input name="__RequestVerificationToken" type="hidden" value="BpCrfBSn3mnAzts6mcvGl33mhhY9bawCPngHKjmHhawxZF9875btEE33VXTb_t4LiXzv94pjPkpnHT5r5hQIQCy1yy_odPvZbqwttZS7KNM1"><input name="waybillType" id="waybillType" type="hidden" value="0"><input name="waybill" id="waybill" type="hidden" value="[TRACK_CODE]"></form>',
            ),
            'DESPATCH_BAY'     => array (
                'name'      => 'Despatch bay',
                'track_url' => 'https://despatchbay.com/tracking?tracking-number=[TRACK_CODE]',
            ),
            'COLIS'            => array (
                'name'      => 'Colis privè',
                'track_url' => 'https://www.colisprive.com/moncolis/Default.aspx?numColis=[TRACK_CODE]',
            ),
            'SAPO'             => array (
                'name'      => 'South African Post Office (SA Post office)',
                'track_url' => 'https://tracking.postoffice.co.za/TrackNTrace/TrackNTrace.aspx?id=[TRACK_CODE]',
            ),
            'GLS'              => array (
                'name'      => 'GLS',
                'track_url' => 'https://gls-group.eu/DE/en/parcel-tracking?match=[TRACK_CODE]',
            ),
            'ABX'              => array (
                'name'      => 'ABX Express',
                'track_url' => 'http://www.abxexpress.com.my/track.asp?vsearch=True&tairbillno=[TRACK_CODE]',
            ),
            'EMS'              => array (
                'name'      => 'EMS China Courier',
                'track_url' => 'http://www.ems.com.cn/ems/order/singleQuery_e?mailNum=[TRACK_CODE]',
            ),
            'CORREOS'          => array (
                'name'      => 'Correos',
                'track_url' => 'http://www.correos.es/ss/Satellite/site/pagina-localizador_referencia_expedicion/sidioma=en_GB&Referencia=[TRACK_CODE]',
            ),
            'YURTICI_KARGO'    => array (
                'name'      => 'Yurtici Kargo',
                'track_url' => 'http://www.yurticikargo.com/bilgi-servisleri/sayfalar/kargom-nerede.aspx?q=[TRACK_CODE]',
            ),
            'POST_HASTE'       => array (
                'name'      => 'Post Haste',
                'track_url' => 'http://www.posthaste.co.nz/phl/servlet/ITNG_TAndTServlet?page=1&Key_Type=Ticket&VCCA=Enabled&product_code=[TRACK_CODE][0]&serial_number=[TRACK_CODE][1]',
            ),
            'ICS'              => array (
                'name'      => 'ICS Courier',
                'track_url' => 'http://www.icscourier.ca/online-services/tracking.aspx?trackNums=[TRACK_CODE]',
            ),
            'YAMATO'           => array (
                'name'      => 'Yamato transport',
                'track_url' => 'etrace.9625taqbin.com/gli_trace/GDXTX010S10Action_doInit.action?jvCd=70201&dkbn=1&number=[TRACK_CODE]',
            ),
            'DELHIVERY'        => array (
                'name'      => 'Delhivery',
                'track_url' => 'http://track.delhivery.com/api/status/packages/json/?waybill=[TRACK_CODE]&callback=jQuery1102007566498895175755_1421768265347&_=1421768265349',
            ),
            'EL_CORREO'        => array (
                'name'      => 'El correo',
                'track_url' => 'http://www.elcorreo.com.gt/cdgcorreo/internacional/formulario.php',
            ),
            'DEPRISA'          => array (
                'name'      => 'Deprisa',
                'track_url' => 'http://www.deprisa.com//Tracking/index/?track=[TRACK_CODE]',
                'form'      => '<form name="DEPRISA" target="_blank" method="post" action="http://www.kdz.com/en/track-and-trace/"><input name="nr" type="hidden" value="[TRACK_CODE]"></form>',
            ),
            'TRANS_MISSION'    => array (
                'name'      => 'TransMission.NL',
                'track_url' => 'http://www.mijnzending.nl/portal/index.php?view=tt/vAnoniem&zendingnummer=[TRACK_CODE][0]&postcode=[TRACK_CODE][1]',
            ),
            'SDA_IT'           => array (
                'name'      => 'SDA Italy',
                'track_url' => 'http://wwww.sda.it/SITO_SDA-WEB/dispatcher?id_ldv=[TRACK_CODE]&invoker=home&LEN=&execute2=ActionTracking.doGetTrackingHome&button=Vai',
            ),
            'SAGAWA_EX'        => array (
                'name'      => 'SAGAWA EXPRESS',
                'track_url' => 'http://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijosearcheng.jsp',
            ),
            'TOLLGROUP'        => array (
                'name'      => 'Toll Priority | Toll Group',
                'track_url' => 'https://online.toll.com.au/trackandtrace/',
            ),
            'THAILAND_POST'    => array (
                'name'      => 'Thailand Post',
                'track_url' => 'http://track.thailandpost.co.th/tracking/default.aspx',
            ),
            'CHILEXPRESS'      => array (
                'name'      => 'Chilexpress',
                'track_url' => 'https://www.chilexpress.cl/Views/ChilexpressCL/Resultado-busqueda.aspx?DATA=[TRACK_CODE]',
            ),
            'POSTNET_CO_ZA'    => array (
                'name'      => 'Postnet.co.za',
                'track_url' => 'http://www.courierit.co.za/Trackit/Trackit.aspx?WaybillNumber=[TRACK_CODE]&AccountNumber=TRACKIT',
            ),
            'DAWNWING'         => array (
                'name'      => 'Dawnwing.co.za',
                'track_url' => 'http://www.dawnwing.co.za/business-tools/online-parcel-tracking/',
                'form'      => '<form name="DAWNWING" target="_blank" action="http://www.dawnwing.co.za/business-tools/online-parcel-tracking/" method="post"><input name="WaybillNo" type="hidden" value="[TRACK_CODE]"></form>',
            ),
            'CYPRUS_POST'      => array (
                'name'      => 'Cyprus POST',
                'track_url' => 'http://ips.cypruspost.gov.cy/IPSWEBTRACK/IPSWeb_item_events.asp?itemid=[TRACK_CODE]&Submit=%CE%A5%CF%80%CE%BF%CE%B2%CE%BF%CE%BB%CE%AE+%2F+Submit',
            ),
            'CAINIAO'          => array (
                'name'      => 'CAINIAO',
                'track_url' => 'http://global.cainiao.com/detail.htm?mailNo=[TRACK_CODE]',
            ),
            '17TRACK'          => array (
                'name'      => '17TRACK ALL-IN-ONE PACKAGE TRACKING',
                'track_url' => 'http://www.17track.net/en/result/post-details.shtml?nums=[TRACK_CODE]',
            ),
            'WEDOEXPRESS'      => array (
                'name'      => 'WEDOEXPRESS',
                'track_url' => 'http://www.wedoexpress.com/en/track/s-rest.html?carrier=wedo&nums=[TRACK_CODE]',
            ),
            'GLS_NL'           => array (
                'name'      => 'GLS Netherlands',
                'track_url' => 'http://www.gls-info.nl/tracking',
                'form'      => '<form name="GLS_NL" target="_blank" action="http://www.gls-info.nl/tracking" method="post"><input type="hidden" name="ParcelNo" id="tracking_main_parcelno" value="[TRACK_CODE]"><input type="hidden" name="Zipcode" id="tracking_main_zipcode" value="Postcode"></form>',
            ),
            'ACS'              => array (
                'name'      => 'ACS courier',
                'track_url' => 'https://www.acscourier.net',
                'form'      => '<form name="ACS" target="_blank" id="trackingQuery" action="https://www.acscourier.net/en/track-and-trace?p_p_id=ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet&amp;p_p_lifecycle=1&amp;p_p_state=normal&amp;p_p_mode=view&amp;p_p_col_id=column-4&amp;p_p_col_pos=1&amp;p_p_col_count=2&amp;_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_javax.portlet.action=trackTrace" method="POST"> <input type="hidden" name="jspPage" value="TrackTraceController"><input id="generalCode" name="generalCode" type="hidden" value="[TRACK_CODE]"></form>',
            ),
            'CHRONOPOST_FR'    => array (
                'name'      => 'Chronopost.fr',
                'track_url' => 'http://www.chronopost.fr/fr/chrono_suivi_search#[TRACK_CODE]',
            ),
            'FASTWAY_COM_AU'   => array (
                'name'      => 'Fastway.com.au',
                'track_url' => 'http://fastway.com.au/courier-services/track-your-parcel?l=[TRACK_CODE]',
            ),
            'XPO'              => array (
                'name'      => 'XPO Logistics, Inc.',
                'track_url' => 'https://www.con-way.com/webapp/manifestrpts_p_app/shipmentTracking.do',
                'form'      => '<form name="XPO" target="_blank" method="post" action="https://www.con-way.com/webapp/manifestrpts_p_app/shipmentTracking.do"><input type="hidden" name="loggedInFlag" value="N"><input type="hidden" name="submitAction.x" value="12"><input type="hidden" name="submitAction.y" value="24"><input type="hidden" name="trackingNumbers" value="[TRACK_CODE]"></form>',
            ),
            'OVERNITE_DUBAI'   => array (
                'name'      => 'Overnite Dubai',
                'track_url' => 'http://overnitedubai.com/tracking.php',
                'form'      => '<form name="OVERNITE_DUBAI" target="_blank" method="post" action="http://overnitedubai.com/tracking.php"><input type="hidden" name="tracking_no" value="[TRACK_CODE]"></form>',
            ),
            'COORDINADORA'     => array (
                'name'      => 'Coordinadora',
                'track_url' => 'http://www.coordinadora.com',
            ),
            'DTDC'             => array (
                'name'      => 'DTDC',
                'track_url' => 'http://dtdc.in/tracking/tracking_results.asp',
                'form'      => '<form name="DTDC" target="_blank" method="post" action="http://dtdc.in/tracking/tracking_results.asp"><input type="hidden" name="action" value="track"><input type="hidden" name="sec" value="tr"><input type="hidden" name="ctlActiveVal" value="1"><input type="hidden" name="Ttype" value="awb_no"><input type="hidden" name="strCnno" value="Z76596967"><input type="hidden" name="GES" value="N"><input name="TrkType2" id="TrkType2" type="hidden" value="awb_no"><input name="strCnno2" value="[TRACK_CODE]"></form>',
            ),
            'ECOM_EXPRESS'     => array (
                'name'      => 'Ecom Express',
                'track_url' => 'https://billing.ecomexpress.in/track_me/multipleawb_open/?awb=&order=[TRACK_CODE]&news_go=track+now',
            ),
            'PACKLING_ES'      => array (
                'name'      => 'Packlink.es',
                'track_url' => 'http://www.packlink.es',
                'form'      => '<form name="PACKLING_ES" target="_blank" method="post" action="http://www.packlink.es/es/seguimiento-envios/"><input class="inputTrack" id="num" name="num" type="hidden" value="[TRACK_CODE]"></form>',
            ),
            'SEUR'             => array (
                'name'      => 'Seur.com',
                'track_url' => 'http://www.seur.com',
                'form'      => '<form name="SEUR" target="_blank" method="post" action="http://www.seur.com/seguimiento-online.do" id="seguimientoOnlineForm"><input type="hidden" name="segOnlineAmbito" value="internacional"><input type="hidden" value="[TRACK_CODE]" name="segOnlineIdentificador"></form>',
            ),
            'MRW'              => array (
                'name'      => 'MRW Espana',
                'track_url' => 'http://www.mrw.es/seguimiento_envios/MRW_resultados_consultas.asp?modo=nacional&envio=[TRACK_CODE]',
            ),
            'ENVIALIA'         => array (
                'name'      => 'Envialia',
                'track_url' => 'http://www.envialia.com',
                'form'      => '<form name="ENVIALIA" target="_blank" method="post" id="formulario" action="http://www.envialia.com/index.php?option=com_envios"><input type="hidden" id="envio" name="filtro" value="envio"><input type="hidden" id="numero" name="dato" value="[TRACK_CODE]"><input type="hidden" id="inicial" name="fechaIni" value=""><input type="hidden" id="final" name="fechaFin" value="><input type="hidden" id="cp" name="codPostal"></form>',
            ),
            'ZELERIS'          => array (
                'name'      => 'Zeleris',
                'track_url' => 'http://www.zeleris.com',
                'form'      => '<form name="zeleris" method="post" id="frm" action="https://www.zeleris.com/seguimiento_envio.aspx"><input name="txtTab1IdSeguimiento" type="hidden" value="[TRACK_CODE]" id="txtTab1IdSeguimiento"><input type="hidden" name="_LASTFOCUS" value=""><input type="hidden" name="__EVENTTARGET" value="lnkTab1Consultar"><input type="hidden" name="__EVENTARGUMENT" value=""><input type="hidden" name="__VIEWSTATE" value = "/wEPDwUJMzM5NjA2NjIwD2QWAgIDD2QWDgIBD2QWCgIKDw8WAh4HRW5hYmxlZGhkZAIMDw8WBB4LTmF2aWdhdGVVcmxlHgZUYXJnZXRlZGQCDg8PFgQfAQUQfi9Vc2VyTG9naW4uYXNweB4EVGV4dAUPQWNjZXNvIHVzdWFyaW9zZGQCGg8WAh4LXyFJdGVtQ291bnQCAhYEZg9kFgICAw8PFgYfAQUMfi9pbmRleC5hc3B4HwJlHwMFBkluaWNpb2RkAgIPZBYEAgEPDxYEHwMFFlNlZ3VpbWllbnRvIGRlIGVudsOtb3MeB1Zpc2libGVnZGQCAw8PFgIfBWhkZAIcDw8WAh8DBRZTZWd1aW1pZW50byBkZSBlbnbDrW9zZGQCAw8WAh4FY2xhc3MFDnByaW1lcm8gYWN0aXZvFgICAQ8PFgIeDU9uQ2xpZW50Q2xpY2sFGGphdmFzY3JpcHQ6cmV0dXJuIGZhbHNlO2RkAgUPFgIfBmUWAgIBDw8WAh8HZWRkAgcPDxYCHwVnZBYCAgcPDxYGHghDc3NDbGFzcwUJbXNnX2Vycm9yHwNlHgRfIVNCAgJkZAILDw8WAh8FaGQWAgIDD2QWBGYPZBYCAhsPPCsADQBkAgIPZBYCAikPZBYCAgEPZBYCAgEPEGRkFgFmZAIND2QWAgIDD2QWBGYPZBYCAhUPPCsADQBkAgIPZBYCAh8PZBYCAgEPZBYCAgEPEGRkFgFmZAIPD2QWAgIDD2QWBAIPD2QWAgIBD2QWAmYPZBYCAgEPPCsADQBkAhEPZBYCAgEPZBYCZg9kFgICAQ88KwANAGQYBAUhY3RsRGV0YWxsZU5vQWJvbmFkb3MkZ3JkX2Rlc3Rpbm9zD2dkBSVjdGxEZXRhbGxlTm9BYm9uYWRvcyRncmRfaGlzdG9yaWFsUkNPD2dkBSNjdGxEZXRhbGxlUmVjb2dpZGEkZ3JkX2hpc3RvcmlhbFJDTw9nZAUiY3RsRGV0YWxsZUV4cGVkaWNpb24kZ3JkX2hpc3RvcmlhbA9nZAV0g2YTMXJGZ6YC6eUBq6JGNctV" <input type="hidden" name="__VIEWSTATEGENERATOR" value="1A6E8B63"><input type="hidden" name="__EVENTVALIDATION" value="/wEWBgLw3vjtAQKfsa/zAwLw7IizAgLbnfX/CALx9s+nDgKDwte1A7mOhCNnil2XO5Ue4fstDaGgKle5"></form>',
            ),
            'NACEX'            => array (
                'name'      => 'Nacex',
                'track_url' => 'http://www.nacex.es',
            ),
            'ASM'              => array (
                'name'      => 'ASM',
                'track_url' => 'http://es.asmred.com/',
            ),
            'TOURLINE_EXP'     => array (
                'name'      => 'Tourline Express',
                'track_url' => 'http://www.tourlineexpress.com/default.aspx',
            ),
            'KEAVO'            => array (
                'name'      => 'KEAVO',
                'track_url' => 'http://www.keavo.com',
            ),
            'MONDIAL_RELAY'    => array (
                'name'      => 'Mondial Relay',
                'track_url' => 'http://www.puntopack.es',
            ),
            'STARTPACK'        => array (
                'name'      => 'Starpack',
                'track_url' => 'http://www.packlink.es',
                'form'      => '<form name="PACKLING_ES" target="_blank" method="post" action="http://www.packlink.es/es/seguimiento-envios/"><input class="inputTrack" id="num" name="num" type="hidden" value="[TRACK_CODE]"></form>',
            ),
            'ISRAEL_POST'      => array (
                'name'      => 'Israel Post',
                'track_url' => 'http://www.israelpost.co.il',
            ),
        );

        protected static $instance;

        /**
         * Returns single instance of the class
         *
         * @since 1.0.0
         */
        public static function get_instance () {
            if ( is_null ( self::$instance ) ) {
                self::$instance = new self();
                self::$instance->init_custom_parameters ();
            }

            return self::$instance;
        }

        /**
         * Constructor
         *
         * Initialize plugin and registers actions and filters to be used
         *
         * @since  1.0
         * @author Lorenzo Giuffrida
         */
        protected function __construct () {

        }

        /**
         * Replace custom parameter from the carrier list, adding additional field to track urls as stated on external 'yith-carriers-parameters.php' file
         */
        private function init_custom_parameters () {

            if ( file_exists ( YITH_YWOT_DIR . 'yith-carriers-parameters.php' ) ) {
                require_once ( YITH_YWOT_DIR . 'yith-carriers-parameters.php' );

                foreach ( $carriers_parameter as $key => $value ) {
                    foreach ( $value as $key2 => $value2 ) {
                        $carrier          = &$this->carriers_list[ $key ];
                        $carrier[ $key2 ] = $value2;
                    }
                }
            }
        }

        /**
         * Compare carrier by name starting from array(
         * 'CARRIER_KEY' => array (
         *      'name'      => 'CARRIER FULL NAME',
         *       'track_url' => 'CARRIER TRACKING URL',
         * ),
         * ...
         *
         */
        private function compare_carriers ( $carrier1, $carrier2 ) {
            return strcasecmp ( $carrier1[ 'name' ], $carrier2[ 'name' ] );
        }

        /**
         * Get carriers list supported by plugin
         *
         * @param bool $show_hidden ask if hidden carriers should be shown
         *
         * @return array
         */
        public function get_carriers_list ( $show_hidden = true ) {

            $list        = $this->carriers_list;
            $hidden_list = array ();

            foreach ( $list as $key => $value ) {

                if ( in_array ( $key, $this->carriers_hidden ) ) {
                    $hidden_list[ $key ] = $value;
                    unset( $list[ $key ] );
                }
            }

            // sort alphabetically by name
            uasort ( $list, array ( $this, 'compare_carriers' ) );

            if ( $show_hidden ) {
                return array_merge ( $hidden_list, $list );
            }

            return $list;
        }

        /**
         * Compare string
         */
        private function compare_name ( $name1, $name2 ) {
            return strcasecmp ( $name1, $name2 );
        }

        /**
         * Get all carriers set as in use from the global carriers list
         *
         * @param bool $show_hidden true if hidden carriers should be added to result
         *
         * @return array    all enabled carriers
         */
        public function get_carriers_enabled ( $show_hidden = false ) {

            $carriers_enabled             = get_option ( 'ywot_carriers' );
            $carriers_enabled_list        = array ();
            $carriers_enabled_hidden_list = array ();

            foreach ( $this->carriers_list as $key => $value ) {

                if ( isset( $carriers_enabled[ $key ] ) ) {
                    $carriers_enabled_list[ $key ] = $value[ 'name' ];
                } else if ( $show_hidden && in_array ( $key, $this->carriers_hidden ) ) {
                    $carriers_enabled_hidden_list[ $key ] = $value[ 'name' ];
                }
            }

            uasort ( $carriers_enabled_list, array ( $this, 'compare_name' ) );

            return array_merge ( $carriers_enabled_hidden_list, $carriers_enabled_list );
        }
    }
}
