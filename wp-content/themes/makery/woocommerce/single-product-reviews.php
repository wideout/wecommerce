<?php
/*
@version 3.0.0
*/

if(!defined('ABSPATH')) {
    exit;
}

global $product;

if (!comments_open()) {
	return;
}	
?>
<div id="reviews" class="item-reviews">
	<div>
		<h1 class="GothamBold"><?php _e('See what people are saying now', 'makery'); ?></h1>
		<hr class="product_page_hr">
	</div>


	<?php if(get_option('woocommerce_review_rating_verification_required')=== 'no' || wc_customer_bought_product('', get_current_user_id(), $product->id)){ ?>
	 <div class="reviews-form">
		<a href="#review_form" class="element-button element-colorbox"><?php _e('Add Review', 'makery'); ?></a>
	</div> 
	<div class="site-popups hidden">
		<div id="review_form">
			<div class="site-popup large">
				<div class="site-form">
					<?php
					$commenter=wp_get_current_commenter();

					$comment_form=array(
						'title_reply' => '',
						'title_reply_to' => '',
						'comment_notes_before' => '',
						'comment_notes_after' => '',
						'fields' => array(
							'author' => '<div class="column fourcol static"><label for="author">'.__('Name', 'makery').'</label></div><div class="eightcol column static last"><div class="field-wrap"><input id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'" size="30" aria-required="true" /></div></div>',
							'email' => '<div class="column fourcol static"><label for="email">'.__('Email', 'makery').'</label></div><div class="eightcol column static last"><div class="field-wrap"><input id="email" name="email" type="text" value="'.esc_attr($commenter['comment_author_email']).'" size="30" aria-required="true" /></div></div>',
						),
						'label_submit' => __('Add Review', 'makery'),
						'name_submit' => 'submit',
						'class_submit' => '',
						'logged_in_as' => '',
						'comment_field' => '',
					);

					/*if(get_option('woocommerce_enable_review_rating')=== 'yes'){
						$comment_form['comment_field']='<div class="column fourcol static"><label for="rating">'.__('Rating', 'makery').'</label></div>
						<div class="column eightcol static last"><div class="element-select"><span></span>
						<select name="rating" id="rating">
							<option value="">&ndash;</option>
							<option value="5">'.__('Perfect', 'makery').'</option>
							<option value="4">'.__('Good', 'makery').'</option>
							<option value="3">'.__('Average', 'makery').'</option>
							<option value="2">'.__('Not that bad', 'makery').'</option>
							<option value="1">'.__('Very Poor', 'makery').'</option>
						</select></div></div><div class="clear"></div>';
					}*/

					if(get_option('woocommerce_enable_review_rating')=== 'yes'){
						$comment_form['comment_field']='<div class="column fourcol static"><label for="rating">'.__('Rating', 'makery').'</label></div>
						<div class="column eightcol static last"><div class="element-select"><span></span>
						<select name="rating" id="rating">
							<option value="">&ndash;</option>
							<option value="5">'.__('Youtube', 'makery').'</option>
							<option value="4">'.__('Instagram', 'makery').'</option>
						
						</select></div></div><div class="clear"></div>';
					}


					/*if(get_option('woocommerce_enable_review_rating')=== 'yes'){
						$comment_form['comment_field']='<div class="column fourcol static"><label for="rating">'.__('Channel', 'makery').'</label></div>
						<div class="column eightcol static last"><div class="element-select"><span></span>
						<select name="rating" id="rating">
							<option value="">&ndash;</option>
							<option value="youtube">'.__('Youtube', 'makery').'</option>
							<option value="instagram">'.__('Instagram', 'makery').'</option>							
						</select></div></div><div class="clear"></div>';
					}*/

					$comment_form['comment_field'].= '<textarea id="comment" name="comment" cols="45" rows="6" aria-required="true" placeholder="'.__('Review', 'makery').'"></textarea>';
					comment_form(apply_filters('woocommerce_product_review_comment_form_args', $comment_form));
					?>
				</div>
			</div>
		</div>
	</div>
	<!-- /popups -->
	<?php } else { ?>
		<p class="woocommerce-verification-required secondary"><?php _e('Only logged in customers who have purchased this product may leave a review.', 'makery'); ?></p>
	<?php } ?>

	<?php if(have_comments()) { ?>
			<ul>
				<?php 
				$comments = get_comments(array(
					'post_id' => $product->id,
					'status' => 'approve' //Change this to the type of comments to be displayed
				));

				$main_comments = get_comments(array(
					'post_id' => $product->id,
					'number' => 1,
					'status' => 'approve' //Change this to the type of comments to be displayed
				));


				

				$user_comment_count = get_comments(array(
					'post_id' => $product->id,
					'user_id' => get_current_user_id(),
					'status' => 'hold', //Change this to the type of comments to be displayed
					'count' => 'true'
				));

				?>
				<?php 
					if($user_comment_count != 0)
					{
					?>`
						<p class="woocommerce-noreviews secondary"><?php _e('Submitted Review will be subjected for approval. Thank you!', 'makery'); ?></p>
					<?php
					}
						
						
						//wp_list_comments(apply_filters('woocommerce_product_review_list_args', array('callback' => 'woocommerce_comments')), $comments); 
				?>
			</ul>
			<?php if(get_comment_pages_count() > 1 && get_option('page_comments')) { ?>
			<nav class="pagination">
				<?php
				paginate_comments_links(apply_filters('woocommerce_comment_pagination_args', array(
					'prev_text' => '',
					'next_text' => '',
					'type' => 'plain',
				)));
				?>
			</nav>
			<?php } ?>
		<?php } else { ?>
		<!-- <p class="woocommerce-noreviews secondary"><?php _e('There are no reviews yet.', 'makery'); ?></p> -->
		<?php } ?>
		<div class="clear"></div>
	</div>

	<br>
	<div id="comments" class="comments-wrap">
	


<?php 
		
		//Main Review
		if (!empty($main_comments))
		{
			
		

		
		foreach ($main_comments as $comment) {	

			?>

				
					  <?php 
					  if (preg_match('/<iframe.*src=\"(.*)\".*><\/iframe>/isU', $comment->comment_content, $matches)) {
							echo $iframe = $matches[0];
							$content = str_replace($iframe, '', $comment->comment_content);
							?>
							<div class="prod_review">
								<div class="prod_slide">
									<?php echo $iframe; ?>
								</div>
							  <div class="avatar_img"><img src="http://wecommerce.ph/v3/wp-content/uploads/2016/05/default-avatar-250x250.png"></div>
							</div>
							<div class="prod_content">
								<?php echo $content; ?>
								
							</div>
							<?php

						}
						
						else if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $comment->comment_content, $match)) { 
							$video_id = $match[1];
							$iframe = '<iframe src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe>';
							
							$content = str_replace('https://www.' . $match[0], '', $comment->comment_content);
						
							?>
							<div class="prod_review">
								<div class="prod_slide">
									<?php echo $iframe; ?>
								</div>
							  <div class="avatar_img"><img src="http://wecommerce.ph/v3/wp-content/uploads/2016/05/default-avatar-250x250.png"></div>
							</div>
							<div class="prod_content">
								<?php echo $content; ?>
								
							</div>
							<?php 

						}

						else if (preg_match('/https?:\/\/[w\.]*instagram\.[^\/]*\/([^?]*)/is', $comment->comment_content, $images)) {
							//var_dump($images);
							preg_match('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(\/\S*)?/', $comment->comment_content, $url);
							
							//$content = str_replace($url[0], '', $row['post_content']);	
							
							$api = file_get_contents("https://api.instagram.com/oembed/?url=" . $url[0]);      
							$apiObj = json_decode($api,true);     
							$thumbnail_url = $apiObj['thumbnail_url'];
							$content = $apiObj['title'];
							?>

							<div class="prod_review">
								<div class="prod_slide">
									<img src="<?php echo $thumbnail_url; ?>">
								</div>
							  <div class="avatar_img"><img src="http://wecommerce.ph/v3/wp-content/uploads/2016/05/default-avatar-250x250.png"></div>
							</div>
							<div class="prod_content">
								<?php echo $content; ?>
								
							</div>							

							
							
							<?php
							//$content = str_replace($image, '', $row['post_content']);		*/		
						}
						else {
							echo $comment->comment_content;
						}

						/*else if (preg_match('/<img.*src=\"(.*)\".*>/isU', $row['post_content'], $images)) {
							
							echo $image = $images[0];
							$content = str_replace($image, '', $row['post_content']);				
						}*/

					   ?>
				   	

				

				<div class="review_buttons">Upvote</div>
				<hr>		

			<?php		
		}
		}
		?>


		<?php if($comments) {  ?>
		<div class="social_holder" style="float: left;"><img src="<?php echo site_url(); ?>/wp-content/uploads/2016/05/YouTube-logo-full_color.png"></div>
		<section class="regular slider inline-block">
		<?php 

			//Youtube Comments

				foreach ($comments as $comment) {				
						
						if (preg_match('/<iframe.*src=\"(.*)\".*><\/iframe>/isU', $comment->comment_content, $matches)) {

							$iframe = $matches[1]; 
							$content = str_replace($matches[0], '', $comment->comment_content);									
							preg_match('/\/v\/(.{11})|\/embed\/(.{11})/', $iframe, $url);
							$video_id = str_replace("/embed/" , "", $url[0]);
							?>
							<div>
						      <img src="http://img.youtube.com/vi/<?php echo $video_id; ?>/0.jpg" ref="<?php echo $video_id; ?>" class="slide_thumb" alt="youtube" data-content="<?php echo $content;?>">
						    </div>
							<?php
							
						}
						else if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $comment->comment_content, $match)) { 
							$video_id = $match[1];    	
							$content = str_replace('https://www.' . $match[0], '', $comment->comment_content);
						
						
							?>
							
							    <div>
							      <img src="http://img.youtube.com/vi/<?php echo $video_id; ?>/0.jpg" ref="<?php echo $video_id; ?>" class="slide_thumb" alt="youtube" data-content="<?php echo $content;?>">
							    </div>
							  
							<?php
						}

					}
		?>
		</section>



		<div class"clear"></div>

		<?php } ?>



		<?php if($comments) {  ?>
		<div class="social_holder" style="float: left;"><img src="<?php echo site_url(); ?>/wp-content/uploads/2016/05/instagram-logo.png"></div>
		<section class="regular slider inline-block">
		<?php 

			//Instagram Comments

				foreach ($comments as $comment) {				
						

						if (preg_match('/https?:\/\/[w\.]*instagram\.[^\/]*\/([^?]*)/is', $comment->comment_content, $images)) {
							//var_dump($images);
							preg_match('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(\/\S*)?/', $comment->comment_content, $url);
							
							//$content = str_replace($url[0], '', $row['post_content']);	
							
							$api = file_get_contents("https://api.instagram.com/oembed/?url=" . $url[0]);      
							$apiObj = json_decode($api,true);     
							$thumbnail_url = $apiObj['thumbnail_url'];
							$content = $apiObj['title'];	

							?>
							
					    	<div>
						    	<img src="<?php echo $thumbnail_url; ?>" ref="<?php echo $thumbnail_url; ?>" class="slide_thumb insta_img" alt="instagram" data-content="<?php echo $content;?>">								
							</div>
							
							
							<?php
							
						}

					}
		?>
		</section>
		<div class"clear"></div>


		<?php } ?>
      
  	</div>
		


	
	<div class="clear"></div>
</div>



<link rel="stylesheet" href="<?php echo THEMEX_URI.'assets/css/'; ?>slick.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo THEMEX_URI.'assets/css/'; ?>slick-theme.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo THEMEX_URI.'assets/js/'; ?>slick.min.js"></script>

<script type="text/javascript">
  	jQuery(document).ready(function($) {
  		$('.regular').slick({
            dots: false,
            arrows: false,
	        infinite: true,
	        slidesToShow: 3,
	        slidesToScroll: 2,
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
              // You can unslick at a given breakpoint now by adding:
              // settings: "unslick"
              // instead of a settings object
            ]
          });
	});
  </script>





  <script type="text/javascript">
  	jQuery( document ).ready(function($) {
  		$( ".slide_thumb" ).click(function() {  			
		   var id = $(this).attr('ref');
		   var type = $(this).attr('alt');		   
		   var content = $(this).attr('data-content');
		  	if (type == "youtube")  {
		  		$('.prod_slide').html('<iframe src="https://www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>');
		  		$('.prod_content').html(content);
		  	}
		  	else if (type == "instagram") {
		  		$('.prod_slide').html('<img src="'+ id +'">');
		  		$('.prod_content').html(content);
		  	}
		});
	   
	});
  </script> 
