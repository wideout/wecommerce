<?php

class RS_JSON_API_Introspector {

  public function get_posts($query = false, $wp_posts = false) {
    global $post, $wp_query;
    $this->set_posts_query($query);
    $output = array();
    while (have_posts()) {
      the_post();
      if ($wp_posts) {
        $new_post = $post;
      } else {
        $new_post = new RS_JSON_API_Post($post);
      }
      $output[] = $new_post;
    }
    return $output;
  }

  public function get_date_archive_permalinks() {
    $archives = wp_get_archives('echo=0');
    preg_match_all("/href='([^']+)'/", $archives, $matches);
    return $matches[1];
  }

  public function get_date_archive_tree($permalinks) {
    $tree = array();
    foreach ($permalinks as $url) {
      if (preg_match('#(\d{4})/(\d{2})#', $url, $date)) {
        $year = $date[1];
        $month = $date[2];
      } else if (preg_match('/(\d{4})(\d{2})/', $url, $date)) {
        $year = $date[1];
        $month = $date[2];
      } else {
        continue;
      }
      $count = $this->get_date_archive_count($year, $month);
      if (empty($tree[$year])) {
        $tree[$year] = array(
          $month => $count
        );
      } else {
        $tree[$year][$month] = $count;
      }
    }
    return $tree;
  }

  public function get_date_archive_count($year, $month) {
    if (!isset($this->month_archives)) {
      global $wpdb;
      $post_counts = $wpdb->get_results("
        SELECT DATE_FORMAT(post_date, '%Y%m') AS month,
               COUNT(ID) AS post_count
        FROM $wpdb->posts
        WHERE post_status = 'publish'
          AND post_type = 'post'
        GROUP BY month
      ");
      $this->month_archives = array();
      foreach ($post_counts as $post_count) {
        $this->month_archives[$post_count->month] = $post_count->post_count;
      }
    }
    return $this->month_archives["$year$month"];
  }

  public function get_categories($args = null) {
    $wp_categories = get_categories($args);
    $categories = array();
    foreach ($wp_categories as $wp_category) {
      if ($wp_category->term_id == 1 && $wp_category->slug == 'uncategorized') {
        continue;
      }
      $categories[] = $this->get_category_object($wp_category);
    }
    return $categories;
  }

  public function get_current_post() {
    global $rs_json_api;
    extract($rs_json_api->query->get(array('id', 'slug', 'post_id', 'post_slug')));
    if ($id || $post_id) {
      if (!$id) {
        $id = $post_id;
      }
      $posts = $this->get_posts(array(
        'p' => $id
      ), true);
    } else if ($slug || $post_slug) {
      if (!$slug) {
        $slug = $post_slug;
      }
      $posts = $this->get_posts(array(
        'name' => $slug
      ), true);
    } else {
      $rs_json_api->error("Include 'id' or 'slug' var in your request.");
    }
    if (!empty($posts)) {
      return $posts[0];
    } else {
      return null;
    }
  }

  public function get_current_category() {
    global $rs_json_api;
    extract($rs_json_api->query->get(array('id', 'slug', 'category_id', 'category_slug')));
    if ($id || $category_id) {
      if (!$id) {
        $id = $category_id;
      }
      return $this->get_category_by_id($id);
    } else if ($slug || $category_slug) {
      if (!$slug) {
        $slug = $category_slug;
      }
      return $this->get_category_by_slug($slug);
    } else {
      $rs_json_api->error("Include 'id' or 'slug' var in your request.");
    }
    return null;
  }

  public function get_category_by_id($category_id) {
    $wp_category = get_term_by('id', $category_id, 'category');
    return $this->get_category_object($wp_category);
  }

  public function get_category_by_slug($category_slug) {
    $wp_category = get_term_by('slug', $category_slug, 'category');
    return $this->get_category_object($wp_category);
  }

  public function get_tags() {
    $wp_tags = get_tags();
    return array_map(array(&$this, 'get_tag_object'), $wp_tags);
  }

  public function get_current_tag() {
    global $rs_json_api;
    extract($rs_json_api->query->get(array('id', 'slug', 'tag_id', 'tag_slug')));
    if ($id || $tag_id) {
      if (!$id) {
        $id = $tag_id;
      }
      return $this->get_tag_by_id($id);
    } else if ($slug || $tag_slug) {
      if (!$slug) {
        $slug = $tag_slug;
      }
      return $this->get_tag_by_slug($slug);
    } else {
      $rs_json_api->error("Include 'id' or 'slug' var in your request.");
    }
    return null;
  }

  public function get_tag_by_id($tag_id) {
    $wp_tag = get_term_by('id', $tag_id, 'post_tag');
    return $this->get_tag_object($wp_tag);
  }

  public function get_tag_by_slug($tag_slug) {
    $wp_tag = get_term_by('slug', $tag_slug, 'post_tag');
    return $this->get_tag_object($wp_tag);
  }

  public function get_authors() {
    global $wpdb;
    $author_ids = $wpdb->get_col("
      SELECT u.ID, m.meta_value AS last_name
      FROM $wpdb->users AS u,
           $wpdb->usermeta AS m
      WHERE m.user_id = u.ID
        AND m.meta_key = 'last_name'
      ORDER BY last_name
    ");
    $all_authors = array_map(array(&$this, 'get_author_by_id'), $author_ids);
    $active_authors = array_filter($all_authors, array(&$this, 'is_active_author'));
    return $active_authors;
  }

  public function get_current_author() {
    global $rs_json_api;
    extract($rs_json_api->query->get(array('id', 'slug', 'author_id', 'author_slug')));
    if ($id || $author_id) {
      if (!$id) {
        $id = $author_id;
      }
      return $this->get_author_by_id($id);
    } else if ($slug || $author_slug) {
      if (!$slug) {
        $slug = $author_slug;
      }
      return $this->get_author_by_login($slug);
    } else {
      $rs_json_api->error("Include 'id' or 'slug' var in your request.");
    }
    return null;
  }

  public function get_author_by_id($id) {
    $id = get_the_author_meta('ID', $id);
    if (!$id) {
      return null;
    }
    return new RS_JSON_API_Author($id);
  }

  public function get_author_by_login($login) {
    global $wpdb;
    $id = $wpdb->get_var($wpdb->prepare("
      SELECT ID
      FROM $wpdb->users
      WHERE user_nicename = %s
    ", $login));
    return $this->get_author_by_id($id);
  }

  public function get_comments($post_id) {
    global $wpdb;
    $wp_comments = $wpdb->get_results($wpdb->prepare("
      SELECT *
      FROM $wpdb->comments
      WHERE comment_post_ID = %d
        AND comment_approved = 1
        AND comment_type = ''
      ORDER BY comment_date
    ", $post_id));
    $comments = array();
    foreach ($wp_comments as $wp_comment) {
      $comments[] = new RS_JSON_API_Comment($wp_comment);
    }
    return $comments;
  }

  public function get_attachments($post_id) {
    $wp_attachments = get_children(array(
      'post_type' => 'attachment',
      'post_parent' => $post_id,
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'suppress_filters' => false
    ));
    $attachments = array();
    if (!empty($wp_attachments)) {
      foreach ($wp_attachments as $wp_attachment) {
        $attachments[] = new RS_JSON_API_Attachment($wp_attachment);
      }
    }
    return $attachments;
  }

  public function get_attachment($attachment_id) {
    global $wpdb;
    $wp_attachment = $wpdb->get_row(
      $wpdb->prepare("
        SELECT *
        FROM $wpdb->posts
        WHERE ID = %d
      ", $attachment_id)
    );
    return new RS_JSON_API_Attachment($wp_attachment);
  }

  public function attach_child_posts(&$post) {
    $post->children = array();
    $wp_children = get_posts(array(
      'post_type' => $post->type,
      'post_parent' => $post->id,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'numberposts' => -1,
      'suppress_filters' => false
    ));
    foreach ($wp_children as $wp_post) {
      $new_post = new RS_JSON_API_Post($wp_post);
      $new_post->parent = $post->id;
      $post->children[] = $new_post;
    }
    foreach ($post->children as $child) {
      $this->attach_child_posts($child);
    }
  }

  protected function get_category_object($wp_category) {
    if (!$wp_category) {
      return null;
    }
    return new RS_JSON_API_Category($wp_category);
  }

  protected function get_tag_object($wp_tag) {
    if (!$wp_tag) {
      return null;
    }
    return new RS_JSON_API_Tag($wp_tag);
  }

  protected function is_active_author($author) {
    if (!isset($this->active_authors)) {
      $this->active_authors = explode(',', wp_list_authors(array(
        'html' => false,
        'echo' => false,
        'exclude_admin' => false
      )));
      $this->active_authors = array_map('trim', $this->active_authors);
    }
    return in_array($author->name, $this->active_authors);
  }

  protected function set_posts_query($query = false) {
    global $json_api, $wp_query;

    if (!$query) {
      $query = array();
    }

    $query = array_merge($query, $wp_query->query);

    if ($json_api->query->page) {
      $query['paged'] = $json_api->query->page;
    }

    if ($json_api->query->count) {
      $query['posts_per_page'] = $json_api->query->count;
    }

    if ($json_api->query->post_type) {
      $query['post_type'] = $json_api->query->post_type;
    }

    if (!empty($query)) {
      query_posts($query);
      do_action('rs_json_api_query', $wp_query);
    }
  }

  /**************************************************************************************************
  * ReadyShipper functions on top of Wordpress JSON API
  **************************************************************************************************/
/*
Get the WooCommerce version installed.
TODO: call once and store the result.
http://wpbackoffice.com/get-current-woocommerce-version-number/
*/
function wpbo_get_woo_version_number() {
        // If get_plugins() isn't available, require it
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
        // Create the plugins folder and file variables
	$plugin_folder = get_plugins( '/' . 'woocommerce' );
	$plugin_file = 'woocommerce.php';
	
	// If the plugin version number is set, return it 
	if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {
		return $plugin_folder[$plugin_file]['Version'];

	} else {
	// Otherwise return null
		return NULL;
	}
} 

  public static function order_status_transform($status) { 
    return('\'wc-' . trim($status, '\'') . '\'');
  }

   /**
    * Returns the timezone string for a site, even if it's set to a UTC offset
    * See:  https://www.skyverge.com/blog/down-the-rabbit-hole-wordpress-and-timezones/
    *
    * Adapted from http://www.php.net/manual/en/function.timezone-name-from-abbr.php#89155
    *
    * @return string valid PHP timezone string
    */
    public function wp_get_timezone_string() {
      // if site timezone string exists, return it
      if ($timezone = get_option('timezone_string'))
          return $timezone;

      // get UTC offset, if it isn't set then return UTC
      if (0 === ($utc_offset = get_option('gmt_offset', 0)))
          return 'UTC';

      // adjust UTC offset from hours to seconds
      $utc_offset *= 3600;

      // attempt to guess the timezone string from the UTC offset
      if ($timezone = timezone_name_from_abbr('', $utc_offset, 0)) {
          return $timezone;
      }

      // last try, guess timezone string manually
      $is_dst = date('I');

      foreach (timezone_abbreviations_list() as $abbr) {
          foreach ($abbr as $city) {
              if ($city['dst'] == $is_dst && $city['offset'] == $utc_offset)
                  return $city['timezone_id'];
          }
      }

      // fallback to UTC
      return 'UTC';
  }

  public function rs_date_with_time_zone($datetime_string) {
      try {
          $datetime = new DateTime($datetime_string, new DateTimeZone($this->wp_get_timezone_string()));
          return $datetime->format('Y-m-d H:i:sP');
      } catch (Exception $e) {
          return $datetime_string;
      }
  }

  public function get_shop_order_stubs($since, $statuses) {
    global $wpdb;

    if (version_compare($this->wpbo_get_woo_version_number(), '2.2', '<')) {

	$query_status_filtered_orders = "SELECT object_id FROM {$wpdb->prefix}term_relationships WHERE term_taxonomy_id IN
                                            (SELECT term_taxonomy_id FROM {$wpdb->prefix}term_taxonomy WHERE term_id IN
		                                (SELECT term_id FROM {$wpdb->prefix}terms WHERE name IN ($statuses)))";
	$query_shop_orders =
		"SELECT ID, post_date, post_excerpt FROM $wpdb->posts
			WHERE ID IN ($query_status_filtered_orders)
			AND post_type = 'shop_order'
			AND post_date > '$since'";
    } else {
        $filtered_order_statuses = implode(",", array_map("RS_JSON_API_Introspector::order_status_transform", explode(",", $statuses)));
	$query_shop_orders =
		"SELECT ID, post_date, post_excerpt FROM $wpdb->posts
			WHERE post_status IN ($filtered_order_statuses) 
			AND post_type = 'shop_order'
			AND post_date > '$since'";
    }


	$shop_order_ids = $wpdb->get_results($query_shop_orders);
    $order_stubs = array();

    foreach ($shop_order_ids as $shop_order_id) {
		$query_order_details = "SELECT meta_key, meta_value FROM {$wpdb->prefix}postmeta
			WHERE post_id = '$shop_order_id->ID'";

		$order_fields = $wpdb->get_results($query_order_details);
		$order_info = array();
		$order_info['id'] = $shop_order_id->ID;
		$order_info['date'] = $this->rs_date_with_time_zone($shop_order_id->post_date);
		$order_info['excerpt'] = $shop_order_id->post_excerpt;
		foreach ($order_fields as $order_field) {
			$order_info[$order_field->meta_key] = $order_field->meta_value;
		}

		//Find line Item IDs and populate that info too
		$query_select_order_item_ids =
			"SELECT order_item_id, order_item_name, order_item_type FROM {$wpdb->prefix}woocommerce_order_items
				WHERE order_id IN ('$shop_order_id->ID')";
		$line_item_ids = $wpdb->get_results($query_select_order_item_ids);

        $line_items = array();

		foreach ($line_item_ids as $line_item_id) {
			$line_item_info = array();
			$line_item_info['item_id'] = $line_item_id->order_item_id;
            // In the past WooCommerce stored encoded HTML entities in this field, which we must decode.
            // https://github.com/woothemes/woocommerce/issues/4124
            // Unfortunately the fix can break strings saved with future versions of WooCommerce as it won't
            // encode entities, and yet we'll try to decode them here. So e.g. if something literally wanted to
            // write &gt; or something like that, it wouldn't work. Ostrich method.
			$line_item_info['item_name'] = html_entity_decode($line_item_id->order_item_name, ENT_QUOTES, 'UTF-8');
			$line_item_info['item_type'] = $line_item_id->order_item_type;
			$query_select_product_details =
				"SELECT meta_key, meta_value FROM {$wpdb->prefix}woocommerce_order_itemmeta
					WHERE order_item_id IN ('$line_item_id->order_item_id')";

			$product_attribs = $wpdb->get_results($query_select_product_details);

            $product_id = '';
            $variation_id = '';
			foreach ($product_attribs as $product_attrib) {
				$line_item_info[$product_attrib->meta_key] = $product_attrib->meta_value;
				if ($product_attrib->meta_key === '_variation_id') {
					$variation_id = $product_attrib->meta_value;
				}
				if ($product_attrib->meta_key === '_product_id') {
					$product_id = $product_attrib->meta_value;
				}
			}
			$pid = ($variation_id === '' || $variation_id === '0') ? $product_id : $variation_id;

			$query_select_product_long_details =
				"SELECT meta_key, meta_value FROM {$wpdb->prefix}postmeta
				WHERE post_id = '$pid'";
			$product_extra_attribs = $wpdb->get_results($query_select_product_long_details);

			foreach ($product_extra_attribs as $product_extra_attrib) {
				$line_item_info[$product_extra_attrib->meta_key] = $product_extra_attrib->meta_value;
			}

			$line_items[] = $line_item_info;
		}
        $order_info['items'] = $line_items;
        $order_stubs[] = $order_info;
    }
	return array('orders' => $order_stubs);
  }

  /**************************************************************************************************
  * ReadyShipper functions on top of Wordpress JSON API -- ends here
  **************************************************************************************************/
}

?>
