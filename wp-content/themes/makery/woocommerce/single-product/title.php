<?php
/*
@version 3.0.0
*/

if(!defined('ABSPATH')) {
    exit;
}
?>

<?php 
global $post; 
$shop=ThemexUser::getShop($post->post_author);		

//echo "<pre>" . print_r($shop,1) . "</pre>";
?>

<a href="<?php echo get_permalink($shop); ?>" class="Majesti" style="color: #9e9e9e; font-size: 18px;"><?php echo get_the_title($shop); ?></a>
<div class="title-holder">
	<h1 itemprop="name" class="product_title entry-title GothamBold"><?php the_title(); ?></h1>
	<?php woocommerce_template_single_rating(); ?>
</div>
<hr class="product_page_hr">