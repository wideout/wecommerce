<?php
/*
 *  Manage additional parameters for carriers on class.carriers.php.
 * Add your parameter as key and modify
 */
$carriers_parameter = array (
    'POST_SWI' => array (
        'track_url' => 'http://www.post.ch/swisspost-tracking?formattedParcelCodes=[TRACK_CODE]&p_language=de',
    ),
);
