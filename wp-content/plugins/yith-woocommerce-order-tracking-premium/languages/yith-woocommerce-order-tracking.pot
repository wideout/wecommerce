#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Order Tracking\n"
"POT-Creation-Date: 2016-01-04 09:56+0100\n"
"PO-Revision-Date: 2015-05-05 14:23+0100\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <plugins@yithemes.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-KeywordsList: __ ;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../class.yith-woocommerce-order-tracking-importer.php:145
msgid "The file does not exist, please try again."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:169
#, php-format
msgid "Failed to import row %s: %s is not a valid order id."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:175
#, php-format
msgid "Failed to import row %s: %s is not a known carrier code."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:191
msgid "The CSV is invalid."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:198
#, php-format
msgid ""
"Operation completed - imported <strong>%s/%s</strong> valid tracking codes."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:230
msgid "Invalid file type"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:280
msgid "Remote server did not respond"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:287
#, php-format
msgid "Remote server returned error response %1$d %2$s"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:295
msgid "Remote file is incorrect size"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:301
msgid "Zero size file downloaded"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:308
#, php-format
msgid "Remote file is too large, limit is %s"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:341
msgid "All done!"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:382
msgid "YITH Order Tracking Importer"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:406
msgid ""
"Hi there! Upload a CSV file containing order's tracking codes to import the "
"contents into your shop. Choose a .csv file to upload, then click \"Upload "
"file and import\"."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:408
msgid "Please look at an example of how the CSV file should look like."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:410
msgid "Click here to download a sample."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:413
msgid "Important note."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:414
msgid ""
"The carrier's code should match one of the carrier's code supported by the "
"plugin . Before starting the import process, please be sure that the carrier "
"code in the CSV file matches one of the supported carriers. See the "
"carrier's code table below for full details."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:429
msgid "Before uploading your import file, you need to fix the following error:"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:441
msgid "Choose a file from your computer:"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:447
#, php-format
msgid "Maximum size: %s"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:453
msgid " OR enter path to file:"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:461
msgid "Delimiter"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:475
msgid "Carrier code"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:476
msgid "Carrier name"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-importer.php:518
msgid "Sorry, an error has occurred . "
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:84
msgid "YITH Order Tracking (CSV)"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:84
msgid "Import tracking codes via csv file."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:123
msgid "Import CSV"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:161
msgid "Order tracking information"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:165
msgid "Vendor"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:166
msgid "Track shipping"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:175
msgid "<b>Current shop</b>"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:258
msgid "Text in emails"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:261
msgid ""
"This is the text of the email that will be sent to the buyer when the order "
"is marked as complete. You can customize the text using the following 3 "
"placeholders, which represent real shipping information."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:262
#: ../plugin-options/general-options.php:46
msgid ""
"Your order has been picked up by [carrier_name] on [pickup_date]. Your track "
"code is [track_code]."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:267
msgid "Position of the text in emails"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:270
#: ../plugin-options/general-options.php:54
msgid ""
"Choose if tracking text have to be shown on top (before order product list) "
"or on bottom (after product list)."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:272
#: ../plugin-options/general-options.php:56
msgid "Show on top"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:273
#: ../plugin-options/general-options.php:57
msgid "Show on bottom"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:279
msgid "Import tracking codes"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:282
msgid ""
"Import tracking codes from a CSV file. The CSV file should be as : [order "
"id];[tracking code];[carrier];[shipping date];[ship status]. All fields are "
"self explanatory, the \"ship status\" is a boolean value stating that the "
"order was shipped"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:379
msgid "Warning"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:380
msgid ""
"The following tracking information are only valid for products of current "
"admin/vendor."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:399
#: ../class.yith-woocommerce-order-tracking.php:665
msgid "Tracking code:"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:402
#: ../class.yith-woocommerce-order-tracking.php:668
msgid "Enter tracking code"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:408
#: ../class.yith-woocommerce-order-tracking.php:674
msgid "Carrier name:"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:420
#: ../class.yith-woocommerce-order-tracking.php:683
msgid "Pickup date:"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:424
#: ../class.yith-woocommerce-order-tracking.php:687
msgid "Enter pick up date"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:431
#: ../class.yith-woocommerce-order-tracking.php:694
msgid "Order picked up"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:464
msgid "Track your order"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:470
msgid "Go to your order page to track the shipping"
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:590
msgid "Track url not found."
msgstr ""

#: ../class.yith-woocommerce-order-tracking-premium.php:658
msgid "Tracking details"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:300
msgid ""
"This order contains shipping costs from more than one seller. Click here to "
"see details."
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:468
msgid "General"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:472
msgid "Carriers"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:474
#: ../class.yith-woocommerce-order-tracking.php:535
msgid "Premium Version"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:532
msgid "Settings"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:560
msgid "Plugin Documentation"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:573
msgid ""
"YITH WooCommerce Order Tracking is available in an outstanding PREMIUM "
"version with many new options, discover it now."
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:574
msgid "Premium version"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:581
msgid "YITH WooCommerce Order Tracking"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:582
msgid ""
"In YIT Plugins tab you can find YITH WooCommerce Order Tracking options. "
"From this menu you can access all settings of YITH plugins activated."
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:632
msgid "Order tracking"
msgstr ""

#: ../class.yith-woocommerce-order-tracking.php:677
msgid "Enter carrier name"
msgstr ""

#: ../init.php:30
msgid ""
"YITH WooCommerce Order Tracking is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""

#: ../plugin-options/carriers-options.php:19
msgid "List of supported carriers"
msgstr ""

#: ../plugin-options/carriers-options.php:20
msgid ""
"Check carriers that will be used for order shipping. <b>If you shouldn't "
"find your favorite carrier in this list, please open a ticket on support."
"yithemes.com. Our support team will be glad to add it as soon as possible.</"
"b>"
msgstr ""

#: ../plugin-options/general-options.php:18
#: ../plugin-options/general-options.php:26
msgid "Default carrier name"
msgstr ""

#: ../plugin-options/general-options.php:21
msgid ""
"  To display the list of carriers, you have to select them first from the "
"specific \"Carriers\" tab that you can find in the top part of the screen."
msgstr ""

#: ../plugin-options/general-options.php:37
msgid "General settings"
msgstr ""

#: ../plugin-options/general-options.php:43
msgid "Text in the Orders page"
msgstr ""

#: ../plugin-options/general-options.php:47
msgid ""
"This is the text showed in Order details page. You can customize the text "
"using the following 3 placeholders, representing real shipping information."
msgstr ""

#: ../plugin-options/general-options.php:51
msgid "Position of the text in the Orders page"
msgstr ""

#: ../templates/admin/carriers.php:13
msgid "Select/Unselect all"
msgstr ""
