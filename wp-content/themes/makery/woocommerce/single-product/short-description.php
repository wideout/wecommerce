<?php
/*
@version 3.0.0
*/

if(!defined('ABSPATH')) {
    exit;
}

global $post;
?>
<div class="item-details" itemprop="description">
	<h6 class="GothamBlack">DESCRIPTION</h6>

	<?php the_content(); ?>
</div>