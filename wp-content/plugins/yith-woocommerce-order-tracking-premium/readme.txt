=== YITH WooCommerce Order Tracking ===

Contributors: yithemes
Tags: woocommerce, order tracking, order track, order trace, delivery, carriers, shipping, ship orders, e-commerce, send product, product shipping, delivery notes, track carriers, track email, delivery note, order shipping, order, orders, shop, e commerce, ecommerce
Requires at least: 4.0
Tested up to: 4.4.1
Stable tag: 1.2.7
License: GPLv2 or later  
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= Version 1.2.7 - RELEASED: FEB 02, 2016 =

* Updated: YODEL DIRECT tracking system

= Version 1.2.6 - RELEASED: FEB 01, 2016 =

* Updated: POST NL tracking system
* Updated: POS Laju tracking system

= Version 1.2.5 - RELEASED: JAN 18, 2016 =

* Added: several spanish carriers( Packlink.es, Seur.com, MRW Espana, Envialia, Zeleris, Nacex, ASM, Tourline Express, KEAVO, Mondial Relay, Starpack)

= Version 1.2.4 - RELEASED: DIC 31, 2015 =

* Added: Bulk import tracking code from CSV file

= Version 1.2.3 - RELEASED: DIC 26, 2015 =

* Added: Overnite Dubai carrier
* Added: Coordinadora carrier
* Added: DTDC carrier

= Version 1.2.2 - RELEASED: DIC 18, 2015 =

* Added: XPO Logistics Inc. carrier

= Version 1.2.1 - RELEASED: DIC 17, 2015 =

* Added: Fastway.com.au carrier

= Version 1.2.0 - RELEASED: NOV 30, 2015 =

* Added: full support to YITH WooCommerce Multi Vendor plugin, with single vendor order tracking
* Updated: Estafeta carrier tracking url

= Version 1.1.8 - RELEASED: NOV 18, 2015 =

* Added: ACS carrier
* Added: Chronopost.fr carrier
* Fixed: direct tracking from emails fails for some carrier
* Fixed: missing gettext for a sentence

= Version 1.1.7 - RELEASED: NOV 17, 2015 =

* Added: global.cainiao.com carrier
* Added: 17track.net carrier
* Added: WEDOEXPRESS carrier
* Added: GLS Netherlands carrier
* Added: Cyprus POST carrier
* Updated: changed YITH plugin framework loading from after_setup_theme hook to plugins_loaded

= Version 1.1.6 - RELEASED: OCT 27, 2015 =

* Added: Fastway.co.za carrier.
* Added: Dawnwing.co.za carrier.
* Added: Supaswift by Fedex carrier.
* Added: Postnet.co.za carrier.
* Updated: SA Post office carrier data.
* Updated: changed text-domain from ywot to yith-woocommerce-order-tracking.

= Version 1.1.5 - RELEASED: SEP 29, 2015 =

* Added: Thailand Post carrier.
* Added: Chilexpress carrier.

= Version 1.1.4 - RELEASED: SEP 17, 2015 =

* Added: SDA Italy carrier and tracking url added.
* Added: SAGAWA EXPRESS carrier and tracking url added.
* Added: 'Toll Priority | Toll Group' carrier and tracking url added.

= Version 1.1.3 - RELEASED: AUG 27, 2015 =

* Tweak: update YITH Plugin framework.

= Version 1.1.2 - RELEASED: JUN 29, 2015 =

* Added: DHL Germany track & trace feature.
* Added: TransMission.NL track & trace feature.

= Version 1.1.1 - RELEASED: JUN 10, 2015 =

* Added: KDZ Express track & trace feature.

= Version 1.1.0 - RELEASED: MAY 05, 2015 =

* Added: support to more carrier website.

= Version 1.0.3 - RELEASED: Mar 04, 2015 =

* Updated: Plugin core framework

= Version 1.0.2 - RELEASED: FEB 17, 2015 =

* Added: Can customize carriers list fields with custom ones.
* Fixed: Date now are shown with wordpress date format.

= Version 1.0.1 - RELEASED: FEB 16, 2015 =

* Fixed: Default carrier sometimes was not correctly selected on new orders

= Version 1.0.0 - RELEASED: FEB 13, 2015 ==

* Initial release