				<?php if(is_active_sidebar('footer')) { ?>
					<div class="clear"></div>
					<div class="footer-sidebar sidebar clearfix">
						<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer')); ?>
					</div>
				<?php } ?>
			</section>
			<!-- /content -->			
		</div>
		<div class="footer-wrap">
			<footer class="site-footer container">
				<div class="site-copyright left">
					<div>
						<img class="social-media-iconset" src="https://wecommerce.ph/wp-content/uploads/2016/03/social-media-icons.png">
					</div>	

					<div>
						<form role="search" method="get" class="woocommerce-product-search" action="https://wecommerce.ph/">
						<input style="float: left;" type="search" class="search-field footer-search" placeholder="Search Products…" value="" name="s" title="Search for:">
						<div class="search-button-footer">></div>
						<inputtype="submit" value="Search">
						<input type="hidden" name="post_type" value="product">
						</form>
					</div>
					<div class="clear"></div>
					<div class="header-logo">
						<a href="<?php echo SITE_URL; ?>" rel="home">
							<img width="35" src="<?php echo ThemexCore::getOption('site_logo', THEME_URI.'images/logo_text.png'); ?>" alt="<?php bloginfo('name'); ?>" />
							<img width="120" src="<?php echo ThemexCore::getOption('site_logo', THEME_URI.'images/logo_pic.png'); ?>" alt="<?php bloginfo('name'); ?>" />
						</a>
					</div>
					
				</div>				
				<div class="site-copyright-spacer"></div>
				<div class="footer-details-right">
				<nav class="footer-menu right">
					<?php wp_nav_menu(array('theme_location' => 'footer_menu')); ?>
					
					
				</nav>
				<br>
								
			
				<div class="company-footer white right center">2016 Wideout Workforces Inc. All rights reserved.</div>
				</div>
			</footer>
			<!-- /footer -->
		</div>
	</div>
	<?php wp_footer(); ?>

	<?php 


	//echo "<pre>" . print_r($post,1) . "</pre>"; 
	if($post->post_type == 'product')
	{
		$vendor_id = $post->post_author;
		global $wpdb;
		$myrows = $wpdb->get_results( "SELECT gtm_code FROM wp_vendor_gtm WHERE vendor_id = " . $vendor_id);
		if(!empty($myrows))
		{
			$gtm_code = $myrows[0]->gtm_code;

		
		?>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id="<?php echo $gtm_code;?>
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','<?php echo $gtm_code;?>');</script>
		<!-- End Google Tag Manager -->	
		<?php


		}
	}


	if(($post->ID == 5) || ($post->ID == 6)) {
		foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item){
			$vendor_id = $cart_item['data']->post->post_author;
			
		}

		if(empty($vendor_id))
		{
			global $vendor_id_thankyou;
			$vendor_id = $vendor_id_thankyou;			
		}

		global $wpdb;
		$myrows = $wpdb->get_results( "SELECT gtm_code FROM wp_vendor_gtm WHERE vendor_id = " . $vendor_id);
		if(!empty($myrows))
		{
			$gtm_code = $myrows[0]->gtm_code;

		
		?>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id="<?php echo $gtm_code;?>
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','<?php echo $gtm_code;?>');</script>
		<!-- End Google Tag Manager -->	
		<?php
		}
		
	}


	if(get_current_user_type() == 'customer') {

		echo "
		<style>

		.profile-menu li:nth-child(1) {
			display: none;
		}


		.profile-menu li:nth-child(5) {
			display: none;
		}

		.profile-menu li:nth-child(6) {
			display: none;
		}
		</style>

		";
	}

	

	?>


	
</body>
</html>